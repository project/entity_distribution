/**
 * @file
 * Attaches entity_distribution behaviors to the entity form.
 */
(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.entityDistFieldsetSummaries = {

    // Summary callbacks.
    summaryCallbacks: [
      function (context, summary) {
        const enabled = $(context).find('input:checkbox[name$="[enable]"]').prop('checked');
        summary = enabled ? Drupal.t('Entity distribution enabled') : Drupal.t('Entity distribution disabled');
        return '<span class="entity-dist-summary">' + summary + '</span>';
      }
    ],

    attach: function (context) {
      $(context).find('.entity-distribution-fieldset').drupalSetSummary(function (context) {
        let summary = '';

        for (const callback_id in Drupal.behaviors.entityDistFieldsetSummaries.summaryCallbacks) {
          console.log(summary);
          summary = Drupal.behaviors.entityDistFieldsetSummaries.summaryCallbacks[callback_id](context, summary);
          console.log(summary);
        }

        return summary;
      });
    }
  };

})(jQuery, Drupal);
