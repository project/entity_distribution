<?php

/**
 * @file
 * Generated by BLT. Serves as an example of global includes.
 */

use Acquia\Blt\Robo\Common\EnvironmentDetector;

// Set site env.
putenv('SITE_ENV=local');

/**
 * An example global include file.
 *
 * To use this file, copy and rename to global.settings.php.
 */

global $_site_name;

/**
 * Site path.
 *
 * @var string $site_path
 * This is always set and exposed by the Drupal Kernel.
 */
if (class_exists(EnvironmentDetector::class)) {
  $_site_name = EnvironmentDetector::getSiteName($site_path);
}
else {
  $_site_name = str_replace('sites/', '', $site_path);
}

/**
 * Include settings files in docroot/sites/settings.
 *
 * If instead you want to add settings to a specific site, see BLT's includes
 * file in docroot/sites/{site-name}/settings/default.includes.settings.php.
 */
$additionalSettingsFiles = [
  // e.g,( DRUPAL_ROOT . "/sites/settings/foo.settings.php" )
  DRUPAL_ROOT . "/sites/settings/secrets.settings.php",
  DRUPAL_ROOT . "/sites/settings/entity_distribution.settings.php"
];

foreach ($additionalSettingsFiles as $settingsFile) {
  if (file_exists($settingsFile)) {
    // phpcs:ignore
    require $settingsFile;
  }
}
