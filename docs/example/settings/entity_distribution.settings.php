<?php

global $_site_name;

$site_env = getenv('SITE_ENV');

// Current website.
if ($_site_name == 'default') {
  // $config['config_split.config_split.content_server']['status'] = TRUE;
  // The current website is machine name of remote entity.
  // $config['entity_distribution.settings']['current_website'] = 'default';
}
elseif ($_site_name == 'japan') {
  // $config['config_split.config_split.content_client']['status'] = TRUE;
  // $config['entity_distribution.settings']['current_website'] = 'japan';
}

// Environment: CI.
if ($site_env == 'ci') {
  // $config['entity_share_client.remote.default']['url'] = 'http://ci.example.com';
  // $config['entity_share_client.remote.japan']['url'] = 'http://ci.japan.example.com';
}
// Environment: Stage.
elseif ($site_env == 'stage') {
  // TODO.
}
// Environment: Prod.
elseif ($site_env == 'prod') {
  // TODO.
}
// Environment: Local, or others.
else {
  // $config['entity_share_client.remote.default']['url'] = 'http://local.example.com';
  // $config['entity_share_client.remote.japan']['url'] = 'http://local.japan.example.com';
}
