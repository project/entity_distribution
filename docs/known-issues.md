
| Issue | Description |
| :--- | :--- |
| Cron interrupt when syncing large content | If you content have a large dependencies chain, the cron may stop working when you syncing this content. <br>It is recommended to using `queue-run entity_share_async_import` to sync content. |
| Queue does not release | Install [Queue UI](https://www.drupal.org/project/queue_ui) and run `drush queue-release entity_share_async_import` command. |
| Patches conflict | Use [`patches-ignore`](https://github.com/cweagans/composer-patches#ignoring-patches) to ignore the conflict patch, then use the patch that you wanted under root composer.json |
