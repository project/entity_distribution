This document explains how to install the Entity Distribution modules on your Content Server website.

#  Configure the Entity Distribution Server (Example: Default)

1. Enable **Entity Distribution Server** on your **Content Server** site.
2. Some steps to enable **Entity Distribution** for content type.

   path: `/admin/structure/types/manage/{content_type}`

   ![content_type_server.png](./images/content_type_server.png)

   1. Register current website.

      ![register_server_website.png](./images/register_server_website.png)

   2. Create channel.

      ![create_channel.png](./images/create_channel.png)

   3. Enable **Entity Distribution**

      ![server_enable.png](./images/server_enable.png)

3. Export the configuration.

   ```text
   - config/default/config_split.config_split.content_server.yml
   - config/default/core.entity_form_display.node.page.default.yml
   - config/default/core.entity_view_display.node.page.default.yml
   - config/default/entity_share_client.remote.default.yml
   - config/default/field.field.node.page.entity_distribution.yml
   - config/default/field.storage.node.entity_distribution.yml
   - config/default/jsonapi_extras.jsonapi_resource_config.node--page.yml
   - config/default/node.type.page.yml
   - config/envs/content_server/.htaccess
   - config/envs/content_server/entity_share_server.channel.content_page.yml
   ```

4. Update the `entity_distribution.settings.php`

   ```php
   $config['config_split.config_split.content_server']['status'] = TRUE;
   ```

   ```php
   $config['entity_distribution.settings']['current_website'] = 'default';
   ```

   ```php
   $config['entity_share_client.remote.default']['url'] = 'http://local.example.com';
   ```

# Step 4: Authorization methods for Website

https://www.drupal.org/docs/contributed-modules/entity-share/authorization-methods

![authorization_methods.png](./images/authorization_methods.png)

1. Example with Key module

   1. Run the following command to install the Key module and Services API Key Authentication module:

   ```shell
   composer require 'drupal/key:^1.15' 'drupal/services_api_key_auth:^3.0@beta'
   ```

   2. Enable these modules.

   ```shell
   drush en key services_api_key_auth -y
   ```

   3. Create apikey auth.

   path: `/admin/config/services/api_key`

   ![apikey_auth.png](./images/apikey_auth.png)

   4. Create apikey storage.

   path: `/admin/config/system/keys`

   ![apikey_storage.png](./images/apikey_storage.png)

   5. Use the apikey storage for website.

   ![apikey_usage.png](./images/apikey_usage.png)

   6. If you think the apikey saved in configuration is unsafe, you can override them in `settings.php`. See the [example files for suggestions](./example/settings/secrets.settings.php)

# Step 5: Site guard

Please exclude the following paths in Site guard.

```text
/jsonapi/*
/*/jsonapi/*
/entity_share
/*/entity_share
/entity-distribution/notify
/*/entity-distribution/notify
```
