To use Entity Distribution, you must install the module on each of the websites in your content network that will be sharing content with each other.

# Requirements

As you prepare to use Entity Distribution with your website, ensure that you plan for the following requirements:

| Application | Component |
| :----- | :----- |
| Drupal version | Drupal 9 or later |
| PHP Dependency manager | Composer |
| PHP version | 7.2 or later |
| Other | Drush 9 or later |

# Install Entity Distribution

Perform the following steps to install the Entity Distribution modules on your website:

1. Clone the website repository locally.

2. From a command prompt window, go to the directory containing your main `composer.json` file.

3. Add repository in your main `composer.json` file.

    ```text
    "repositories": {
        ...
        "drupal/entity_distribution": {
          "type": "git",
          "url": "git@github.com:ciandt-china-dev/drupal-module-entity_distribution.git"
        }
        ...
    }
    ```

4. Run the following command to install the Entity Distribution module and its dependencies:

    ```shell
    composer require drupal/entity_distribution:dev-8.x-1.x
    ```

5. Commit and push the changes to your codebase.

6. Sign in your Drupal website as an administrator.

7. In the **Admin** menu, click **Extend**.

8. Select the check box next to **Entity Distribution**.

9. Click **Install** to start the installation process.

    Optional: you can use `drush` to enable **Entity Distribution**

    ```shell
    drush en entity_distribution -y
    ```

10. Export the configuration

11. Set up the `settings.php`. See the [example files for suggestions](./example/settings/global.settings.php).
