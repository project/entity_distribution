This document explains how to install the Entity Distribution modules on your Content Client website.

# Configure the Entity Distribution Client (Example: Japan)

1. Enable **Entity Distribution Client** on your **Content Client** site.
2. Some steps to enable **Entity Distribution** for content type.

   path: `/admin/structure/types/manage/{content_type}`

   ![content_type_client.png](./images/content_type_client.png)

   1. Register current website.

      ![register_client_website.png](./images/register_client_website.png)

   2. Create import config. See the [example import config for suggestions](./example/settings/entity_share_client.import_config.default.yml).

      ![import_config.png](./images/import_config.png)

      > Please set "Entity reference" to last in "Process entity".

   5. Enable **Entity Distribution**

      ![client_enable.png](./images/client_enable.png)

3. Export the configuration.

   ```text
   - config/default/config_split.config_split.content_client.yml
   - config/default/entity_share_client.remote.japan.yml
   - config/default/node.type.page.yml
   - config/envs/content_client/.htaccess
   - config/envs/content_client/entity_share_client.import_config.default.yml
   ```

4. Update the `entity_distribution.settings.php`

   ```php
   $config['config_split.config_split.content_client']['status'] = TRUE;
   ```

   ```php
   $config['entity_distribution.settings']['current_website'] = 'japan';
   ```

   ```php
   $config['entity_share_client.remote.japan']['url'] = 'http://local.japan.example.com';
   ```
