
Entity Distribution is a decentralized content distribution and discovery service, aims to deliver a multi-site content syndication capability as a part of the Drupal. Customers can use this service to author, search, and share content in a complex network of sites and channels. Entity Distribution syndicates content between your fleet of Drupal sites that have configuration parity.

> Quick Links: [Install](./docs/install.md) | [Content Server](./docs/content-server.md) | [Content Client](./docs/content-client.md) | [Known issues](./docs/known-issues.md) | [Troubleshooting](./docs/troubleshooting.md)

# Key features

Some of the key features of Entity Distribution are:

- Entity Share: Entity Share is a collection of modules allowing you to share content entities like nodes, taxonomy terms, medias etc. between different Drupal instances.
- JSON:API extras: Provides a means to override and provide limited configurations to the default zero-configuration implementation provided by the JSON:API in Core.

# How Entity Distribution works

Entity Distribution creates a content interchage workflow between the different websites in your content network. Each of the websites in your content network owns the content it creates - content that can be shared with any other website in the network. Communication among the websites occurs through the Entity Share service using Point-to-Point rules that regulate how and when the content gets shared between different members of the content network. These rules are called Remotes/Channels.

Entity Distribution enables automated Server updates from content authors to content consumers in near real time through the use of Drupal queues.

Publishers create content on their websites as expected. If a new content item is one of the entity types being shared, then it (and any necessary dependencies) becomes available to client websites to import. Remotes/Channels can be created at a fleet-level to specify which clients should receive which content.

![architecture.png](./docs/images/architecture.png)

| Feature | Description |
| :--- | :--- |
| Remote websites | Manage your websites in remote websites, both content server and client. |
| Channels | A set of contents that you want to access. |
| Import Config | Provides a means to how to import contents, it contain Layout Builder, contain Embed Entity or more. |
| Authorization | Choose which authorization method you want to use to access remote websites. |

# Entity Distribution architecture

A website in a Entity Distribution network can act as a server for some content and a client other content. The original server of a content entity controls the definitive content of the entity, and any changes made by a client website that imported the content entity aren't contributed back.

The main elements provided by Entity Distribution are as follows:

| Element | Description |
| :--- | :--- |
| Entity Distribution | A set of Drupal modules that manages communication with Entity Distribution and helps ou manage how content is published and consumed. The Entity Distribution gets installed on each website in the content network that has to server or client to content, or both. |
| [Entity Share](https://www.drupal.org/project/entity_share) | A collection of modules allowing you to share content entities like nodes, taxonomy terms, medias etc. between different Drupal instances. |
| [JSON:API Extras](https://www.drupal.org/project/jsonapi_extras) | Provides a means to override and provide limited configurations to the default zero-configuration implementation provided by the JSON:API in Core. |
