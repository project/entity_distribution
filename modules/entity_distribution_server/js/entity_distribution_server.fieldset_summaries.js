/**
 * @file
 * Attaches entity_distribution behaviors to the entity form.
 */
(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.entityDistFieldsetSummaries.summaryCallbacks.push(function (context, summary) {
    // Remove server summary to regenerate.
    const $original_summary = $('<div>' + summary + '</div>');
    $original_summary.find('span.entity-dist-server-summary').remove();

    const enabled = $(context).find('input:checkbox[name$="[enable]"]').prop('checked');
    if (!enabled) {
      return $original_summary.html();
    }

    // Regenerate server summary.
    summary = '';

    const $default_channel = $(context).find('select[name$="[default_channel]"]');
    const val = $default_channel.val();

    if (val === '_none' || !val) {
      summary += Drupal.t('Missing default channel');
    }
    else {
      summary += Drupal.t('Default channel: %default_channel', {
        '%default_channel': $default_channel.find(':selected').text(),
      })
    }

    return $original_summary.html() + '<span class="entity-dist-server-summary">' + summary + '</span>';
  });

})(jQuery, Drupal);
