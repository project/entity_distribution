<?php

namespace Drupal\entity_distribution_server\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerInterface;

/**
 * Interface NotifyWorkerInterface.
 *
 * @package Drupal\entity_distribution_server\Plugin\QueueWorker
 */
interface NotifyWorkerInterface extends QueueWorkerInterface, ContainerFactoryPluginInterface {
}
