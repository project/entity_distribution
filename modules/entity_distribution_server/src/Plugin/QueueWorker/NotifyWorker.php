<?php

namespace Drupal\entity_distribution_server\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NotifyWorker.
 *
 * @package Drupal\entity_distribution_server\Plugin\QueueWorker
 *
 * @QueueWorker(
 *   id = "entity_distribution_notify",
 *   title = @Translation("Entity Distribution Notify"),
 *   cron = {"time" = 30}
 * )
 */
class NotifyWorker extends QueueWorkerBase implements NotifyWorkerInterface {

  const QUEUE_NAME = 'entity_distribution_notify';

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The notify helper.
   *
   * @var \Drupal\entity_distribution_server\Helper\NotifyHelperInterface
   */
  protected $notifyHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.channel.entity_distribution');
    $instance->notifyHelper = $container->get('entity_distribution_server.notify_helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!isset($data['source'], $data['channel'], $data['uuids'], $data['langcode'], $data['remote'])) {
      $this->logger->error('Some required fields was missing: source, channel, uuids, langcode, remote.');
      return;
    }

    try {
      $this->notifyHelper->notify(
        $data['source'],
        $data['channel'],
        $data['uuids'],
        $data['langcode'],
        $data['remote']
      );
    }
    catch (\Exception $e) {
      $this->logger->error(
        "Cannot notify @remote to synchronize items (@uuids) from channel @channel of @source with language @langcode:\n@message",
        [
          '@remote' => $data['remote'],
          '@uuids' => implode(', ', $data['uuids']),
          '@channel' => $data['channel'],
          '@source' => $data['source'],
          '@langcode' => $data['langcode'],
          '@message' => $e->getMessage(),
        ]
      );
    }
  }

}
