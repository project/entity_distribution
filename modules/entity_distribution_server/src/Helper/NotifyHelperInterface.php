<?php

namespace Drupal\entity_distribution_server\Helper;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Interface NotifyHelperInterface.
 *
 * @package Drupal\entity_distribution_server\Helper
 */
interface NotifyHelperInterface {

  /**
   * Get the notify info of entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return array
   */
  public function getNotifyInfoOfEntity(ContentEntityInterface $entity);

  /**
   * Notify.
   *
   * @param string $source_id
   *   The source website id.
   * @param string $channel_id
   *   The channel id.
   * @param array $uuids
   *   The uuids.
   * @param string $langcode
   *   The langcode.
   * @param string $remote_id
   *   THe remote website id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   */
  public function notify(string $source_id, string $channel_id, array $uuids, string $langcode, string $remote_id);

  /**
   * Enqueue the notify.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   */
  public function enqueue(ContentEntityInterface $entity);

}
