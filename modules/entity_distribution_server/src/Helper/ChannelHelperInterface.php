<?php

namespace Drupal\entity_distribution_server\Helper;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface ChannelHelperInterface.
 *
 * @package Drupal\entity_distribution_server\Helper
 */
interface ChannelHelperInterface {

  /**
   * Get the default channel.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The entity.
   * @param $throw
   *   Throw error or not.
   *
   * @return \Drupal\entity_share_server\Entity\ChannelInterface|NULL
   */
  public function getDefaultChannel(ConfigEntityInterface $entity, $throw = FALSE);

  /**
   * Get the channels.
   *
   * @param $entity_type
   *   The entity type id.
   * @param $entity_bundle
   *   The entity bundle.
   *
   * @return array
   */
  public function getChannels($entity_type = '', $entity_bundle = '');

}
