<?php

namespace Drupal\entity_distribution_server\Helper;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\entity_distribution\Helper\RemoteHelperInterface;
use Drupal\entity_share_server\Entity\ChannelInterface;

/**
 * Class FormHelper.
 *
 * @package Drupal\entity_distribution_server\Helper
 */
class FormHelper {

  use StringTranslationTrait;

  /**
   * The remote helper.
   *
   * @var \Drupal\entity_distribution\Helper\RemoteHelperInterface
   */
  protected $remoteHelper;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The channel helper.
   *
   * @var \Drupal\entity_distribution_server\Helper\ChannelHelperInterface
   */
  protected $channelHelper;

  /**
   * FormHelper constructor.
   *
   * @param \Drupal\entity_distribution\Helper\RemoteHelperInterface $remote_helper
   *   The remote helper.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\entity_distribution_server\Helper\ChannelHelperInterface $channel_helper
   *   The channel helper.
   */
  public function __construct(RemoteHelperInterface $remote_helper, AccountProxyInterface $current_user, ChannelHelperInterface $channel_helper) {
    $this->remoteHelper = $remote_helper;
    $this->currentUser = $current_user;
    $this->channelHelper = $channel_helper;
  }

  /**
   * Alter the entity form element.
   *
   * @param array $element
   *   The element.
   * @param array $values
   *   The values.
   * @param array $context
   *   The context.
   *
   * @return array
   */
  public function alterEntityFormElement(array &$element, array $values, array $context) {
    $entity = $context['items']->getEntity();

    $element['entity_distribution_server'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Server'),
      '#element_validate' => [[static::class, 'validateEntityFormElement']],
    ];

    $is_default_translation = $entity->isDefaultTranslation();
    if (!$is_default_translation) {
      $default_translation = $entity->getTranslation(LanguageInterface::LANGCODE_DEFAULT);
      $default_translation_values = $default_translation->get('entity_distribution')->first()->getValue();
    }

    $element['entity_distribution_server']['allow_sync'] = [
      '#type' => 'checkbox',
      '#title' => t('Sync to remote sites'),
      '#default_value' => $values['allow_sync'] ?? FALSE,
    ];

    // Allow edit (Untranslatable field).
    $element['entity_distribution_server']['allow_edit'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow edit'),
    ];

    if ($is_default_translation) {
      $element['entity_distribution_server']['allow_edit']['#default_value'] = $values['allow_edit'] ?? TRUE;
    }
    else {
      $element['entity_distribution_server']['allow_edit']['#type'] = 'hidden';
      $element['entity_distribution_server']['allow_edit']['#default_value'] = $default_translation_values['allow_edit'] ?? TRUE;
    }

    // Allow translate (Untranslatable field).
    $element['entity_distribution_server']['allow_translate'] = [
      '#type' => 'checkbox',
      '#title' => t('Allow translate'),
    ];

    if ($is_default_translation) {
      $element['entity_distribution_server']['allow_translate']['#default_value'] = $values['allow_translate'] ?? TRUE;
    }
    else {
      $element['entity_distribution_server']['allow_translate']['#type'] = 'hidden';
      $element['entity_distribution_server']['allow_translate']['#default_value'] = $default_translation_values['allow_translate'] ?? TRUE;
    }

    // Remote websites.
    $element['entity_distribution_server']['remote_websites'] = [
      '#type' => 'details',
      '#title' => t('Remote Websites'),
      'value' => [
        '#type' => 'checkboxes',
        '#default_value' => $values['remote_websites']['value'] ?? [],
        '#options' => array_map(function ($remote) {
          return $remote->label();
        }, $this->remoteHelper->getRemoteWebsites(TRUE)),
      ],
      '#attributes' => [
        'class' => ['entity-distribution-remote-websites']
      ],
    ];

    if ($this->currentUser->hasPermission('administer_remote_entity')) {
      $element['entity_distribution_server']['remote_websites']['#description'] = t('Click <a href=":remote-collection" target="_blank">here</a> to administer remote websites.', [
        ':remote-collection' => Url::fromRoute('entity.remote.collection')->toString(),
      ]);
    }

    $element['#attached']['library'][] = 'entity_distribution_server/form';

    return $element;
  }

  /**
   * Validate entity form element.
   *
   * @param $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateEntityFormElement(&$element, FormStateInterface $form_state) {
    $parents = $element['#parents'];
    array_pop($parents);
    $form_state->setValue($parents, $form_state->getValue($element['#parents']));
  }

  /**
   * Alter entity type form element.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $type
   *   The type.
   * @param array $context
   *   The context.
   *
   * @return array
   */
  public function alterEntityTypeFormElement(array &$element, ConfigEntityInterface $type, array $context) {
    $entity_type = $type->getEntityType();
    $entity_type_id = $entity_type->getBundleOf();
    $entity_bundle = $type->id();

    $channels = $this->channelHelper->getChannels($entity_type_id, $entity_bundle);

    // Messages for channels.
    if (empty($channels)) {
      $element['entity_distribution']['messages']['#message_list']['error'][] = t('Click <a href=":channel-add-form" target="_blank">here</a> to create default channel for this type.', [
        ':channel-add-form' => Url::fromRoute('entity.channel.add_form', [], [
          'query' => [
            'channel_entity_type' => $entity_type_id,
            'channel_bundle' => $entity_bundle,
          ]
        ])->toString(),
      ]);
    }

    // Default channel.
    $element['entity_distribution']['default_channel'] = [
      '#type' => 'select',
      '#title' => t('Default channel'),
      '#options' => array_map(function (ChannelInterface $channel) {
          return $channel->label();
        }, $channels),
      '#default_value' => $type->getThirdPartySetting('entity_distribution', 'default_channel'),
      '#parents' => ['entity_distribution', 'default_channel'],
      '#states' => [
        'required' => [
          ':input[name="entity_distribution[enable]"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name="entity_distribution[enable]"]' => ['checked' => TRUE],
        ],
      ],
      '#element_validate' => [[static::class, 'validateDefaultChannel']],
    ];

    if ($this->currentUser->hasPermission('administer_channel_entity')) {
      $element['entity_distribution']['default_channel']['#description'] = t('Click <a href=":channel-collection" target="_blank">here</a> to administer channel entity.', [
        ':channel-collection' => Url::fromRoute('entity.channel.collection')->toString(),
      ]);
    }

    // Libraries.
    $element['entity_distribution']['#attached']['library'][] = 'entity_distribution_server/fieldset_summaries';

    $element['#entity_builders'][] = [static::class, 'entityTypeFormBuilder'];

    return $element;
  }

  /**
   * Validate the default channel.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateDefaultChannel(array $element, FormStateInterface $form_state) {
    if (!$form_state->getValue(['entity_distribution', 'enable'])) {
      return;
    }

    if (empty($element['#value']) || $element['#value'] == '_none') {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
    }

    $type = $form_state->getFormObject()->getEntity();
    $entity_type = $type->getEntityType();
    $entity_type_id = $entity_type->getBundleOf();
    $entity_bundle = $type->id();

    $default_channel = $element['#value'];
    $channels = \Drupal::service('entity_distribution_server.channel_helper')->getChannels($entity_type_id, $entity_bundle);
    if (!in_array($default_channel, array_keys($channels))) {
      $form_state->setError($element, t('Invalid default channel for this type.'));
    }
  }

  /**
   * The entity type form builder.
   *
   * @param $entity_type
   *   The entity type.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $type
   *   The type.
   * @param $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function entityTypeFormBuilder($entity_type_id, ConfigEntityInterface $type, &$form, FormStateInterface $form_state) {
    // Save config into third party setting.
    $default_channel = $form_state->getValue(['entity_distribution', 'default_channel'], '');
    $type->setThirdPartySetting('entity_distribution', 'default_channel', $default_channel);
  }

}
