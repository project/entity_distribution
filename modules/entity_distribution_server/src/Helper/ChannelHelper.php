<?php

namespace Drupal\entity_distribution_server\Helper;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_share_server\Entity\Channel;
use Drupal\entity_share_server\Entity\ChannelInterface;

/**
 * Class ChannelHelper.
 *
 * @package Drupal\entity_distribution_server\Helper
 */
class ChannelHelper implements ChannelHelperInterface {

  use StringTranslationTrait;

  /**
   * The default channel.
   *
   * @var array
   */
  protected $defaultChannel = [];

  /**
   * The channels.
   *
   * @var array
   */
  protected $channels = [];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ChannelHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultChannel(ConfigEntityInterface $entity, $throw = FALSE) {
    if (!isset($this->defaultChannel[$entity->id()])) {
      $this->defaultChannel[$entity->id()] = $entity->getThirdPartySetting('entity_distribution', 'default_channel') ?: 0;
      $this->defaultChannel[$entity->id()] = Channel::load($this->defaultChannel[$entity->id()]);
    }

    if ($throw && empty($this->defaultChannel[$entity->id()])) {
      throw new \Exception($this->t('Missing default channel on @entity_type:@entity_bundle.', [
        '@entity_type' => $entity->getEntityType()->getBundleOf(),
        '@entity_bundle' => $entity->id(),
      ]));
    }

    return $this->defaultChannel[$entity->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getChannels($entity_type = '', $entity_bundle = '') {
    if (!$this->channels) {
      $this->channels = $this->entityTypeManager->getStorage('channel')->loadMultiple();
    }

    return array_filter($this->channels, function (ChannelInterface $channel) use ($entity_type, $entity_bundle) {
      if ($entity_type && $channel->get('channel_entity_type') !== $entity_type) {
        return FALSE;
      }
      if ($entity_bundle && $channel->get('channel_bundle') !== $entity_bundle) {
        return FALSE;
      }
      return TRUE;
    });
  }

}
