<?php

namespace Drupal\entity_distribution_server\Helper;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\entity_distribution\Helper\RemoteHelperInterface;
use Drupal\entity_distribution_server\Plugin\QueueWorker\NotifyWorker;
use Drupal\entity_share_client\Entity\Remote;
use Drupal\entity_share_client\Service\RemoteManager;
use Drupal\entity_share_client\Service\RemoteManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class NotifyHelper.
 *
 * @package Drupal\entity_distribution_server\Helper
 */
class NotifyHelper implements NotifyHelperInterface {

  /**
   * The http clients.
   *
   * @var array
   */
  protected $httpClients = [];

  /**
   * The remote manager.
   *
   * @var \Drupal\entity_share_client\Service\RemoteManagerInterface
   */
  protected $remoteManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The remote helper.
   *
   * @var \Drupal\entity_distribution\Helper\RemoteHelperInterface
   */
  protected $remoteHelper;

  /**
   * The channel helper.
   *
   * @var \Drupal\entity_distribution_server\Helper\ChannelHelperInterface
   */
  protected $channelHelper;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * NotifyHelper constructor.
   *
   * @param \Drupal\entity_share_client\Service\RemoteManagerInterface $remote_manager
   *   The remote manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\entity_distribution\Helper\RemoteHelperInterface $remote_helper
   *   The remote helper.
   * @param \Drupal\entity_distribution_server\Helper\ChannelHelperInterface $channel_helper
   *   The channel helper.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(RemoteManagerInterface $remote_manager, QueueFactory $queue_factory, LoggerInterface $logger, RemoteHelperInterface $remote_helper, ChannelHelperInterface $channel_helper, EntityTypeManagerInterface $entity_type_manager, ModuleHandlerInterface $module_handler) {
    $this->remoteManager = $remote_manager;
    $this->queueFactory = $queue_factory;
    $this->logger = $logger;
    $this->remoteHelper = $remote_helper;
    $this->channelHelper = $channel_helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Get the http client.
   *
   * @param string $remote_id
   *   The remote id.
   *
   * @return \GuzzleHttp\Client
   */
  protected function getHttpClient(string $remote_id) {
    if (!isset($this->httpClients[$remote_id])) {
      $remote = Remote::load($remote_id);
      $this->httpClients[$remote_id] = $remote->getHttpClient(RemoteManager::JSON_API_CLIENT);
    }
    return $this->httpClients[$remote_id];
  }

  /**
   * Get the notify info of entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity.
   *
   * @return array
   */
  public function getNotifyInfoOfEntity(ContentEntityInterface $entity) {
    $bundle_entity_type_id = $entity->getEntityType()->getBundleEntityType();
    $bundle_entity_type = $this->entityTypeManager->getStorage($bundle_entity_type_id)->load($entity->bundle());
    return [
      'source' => $this->remoteHelper->getCurrentWebsite(TRUE)->id(),
      'channel' => $this->channelHelper->getDefaultChannel($bundle_entity_type, TRUE)->id(),
      'uuids' => [$entity->uuid()],
      'langcode' => $entity->language()->getId(),
    ];
  }

  /**
   * Can notify?
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   */
  public function can(EntityInterface $entity) {
    if (!$entity instanceof ContentEntityInterface || !$entity->hasField('entity_distribution')) {
      return FALSE;
    }

    $can = TRUE;

    $current_website = $this->remoteHelper->getCurrentWebsite();

    // Current website was register.
    if (!$current_website) {
      $can = FALSE;
    }

    $entity_type = $entity->getEntityType();
    $bundle_entity_type_id = $entity_type->getBundleEntityType();
    $bundle_entity_type = $this->entityTypeManager->getStorage($bundle_entity_type_id)->load($entity->bundle());

    // Entity distribution was enabled.
    if (!$bundle_entity_type->getThirdPartySetting('entity_distribution', 'enable')) {
      $can = FALSE;
    }

    // Default channel.
    $default_channel = $this->channelHelper->getDefaultChannel($bundle_entity_type);
    if (!$default_channel) {
      $can = FALSE;
    }

    // Only notify for default revision state.
    if ($this->moduleHandler->moduleExists('content_moderation')) {
      /** @var \Drupal\content_moderation\ModerationInformationInterface $moderation_information */
      $moderation_information = \Drupal::service('content_moderation.moderation_information');
      $workflow = $moderation_information->getWorkflowForEntity($entity);
      if ($workflow && ($state = $workflow->getTypePlugin()->getState($entity->moderation_state->value)) && !$state->isDefaultRevisionState()) {
        $can = FALSE;
      }
    }

    // Metadata.
    $entity_dist_metadata = $entity->get('entity_distribution')->getValue()[0] ?? [];

    if (empty($entity_dist_metadata['allow_sync'])) {
      $can = FALSE;
    }

    $remote_websites = array_filter($entity_dist_metadata['remote_websites']['value'] ?? []);
    if (empty($remote_websites)) {
      $can = FALSE;
    }

    // Invoke hook_entity_distribution_can_notify().
    $cans = $this->moduleHandler->invokeAll('entity_distribution_can_notify', [$entity]);

    $cans[] = $can;

    return !in_array(FALSE, $cans, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function notify(string $source_id, string $channel_id, array $uuids, string $langcode, string $remote_id) {
    $http_client = $this->getHttpClient($remote_id);

    if (!$http_client) {
      throw new \Exception('Missing http client.');
    }

    $guzzle_options = [
      'body' => Json::encode([
        'source' => $source_id,
        'channel' => $channel_id,
        'uuids' => $uuids,
        'langcode' => $langcode,
      ]),
      'allow_redirects' => ['strict' => TRUE],
    ];

    // Not slash starting on uri.
    // @see \GuzzleHttp\Psr7\Uri::isAbsolute(), relative-path references.
    return $http_client->request('POST', 'entity-distribution/notify', $guzzle_options);
  }

  /**
   * {@inheritdoc}
   */
  public function enqueue(ContentEntityInterface $entity) {
    $queue = $this->queueFactory->get(NotifyWorker::QUEUE_NAME);
    $notify_info = $this->getNotifyInfoOfEntity($entity);

    $entity_dist_metadata = $entity->get('entity_distribution')->getValue()[0] ?? [];
    $remote_websites = array_keys(array_filter($entity_dist_metadata['remote_websites']['value'] ?? []));

    foreach ($remote_websites as $remote_website) {
      $queue->createItem(['remote' => $remote_website] + $notify_info);
    }
  }

}
