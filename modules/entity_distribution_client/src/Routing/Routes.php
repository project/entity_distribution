<?php

namespace Drupal\entity_distribution_client\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Authentication\AuthenticationCollectorInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class Routes.
 *
 * @package Drupal\entity_distribution_client\Routing
 */
class Routes implements ContainerInjectionInterface {

  /**
   * The authentication collector.
   *
   * @var \Drupal\Core\Authentication\AuthenticationCollectorInterface
   */
  protected $authCollector;

  /**
   * List of providers.
   *
   * @var string[]
   */
  protected $providerIds;

  /**
   * Instantiates a Routes object.
   *
   * @param \Drupal\Core\Authentication\AuthenticationCollectorInterface $auth_collector
   *   The authentication provider collector.
   */
  public function __construct(AuthenticationCollectorInterface $auth_collector) {
    $this->authCollector = $auth_collector;
  }

  /**
   * Build a list of authentication provider ids.
   *
   * @return string[]
   *   The list of IDs.
   */
  protected function authProviderList() {
    if (isset($this->providerIds)) {
      return $this->providerIds;
    }
    $this->providerIds = array_keys($this->authCollector->getSortedProviders());

    return $this->providerIds;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('authentication_collector')
    );
  }

  /**
   * Provides the notify route.
   */
  public function notify() {
    $collection = new RouteCollection();

    $route_collection = new Route('/entity-distribution/notify', [
      RouteObjectInterface::CONTROLLER_NAME => '\Drupal\entity_distribution_client\Controller\Notify::index',
    ]);

    $route_collection
      ->setRequirement('_permission', 'entity_share_client_pull_content')
      ->setMethods(['POST'])
      ->addOptions([
        '_auth' => $this->authProviderList(),
      ]);

    $collection->add('entity_distribution_client.notify', $route_collection);

    return $collection;
  }

}
