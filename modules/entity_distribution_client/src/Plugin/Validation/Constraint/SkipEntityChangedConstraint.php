<?php

namespace Drupal\entity_distribution_client\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\EntityChangedConstraint as CoreEntityChangedConstraint;

/**
 * Class SkipEntityChangedConstraint.
 *
 * @package Drupal\entity_distribution_client\Plugin\Validation\Constraint
 *
 * @Constraint(
 *   id = "SkipEntityChanged",
 *   label = @Translation("Skip Entity changed", context = "Validation"),
 *   type = {"entity"}
 * )
 */
class SkipEntityChangedConstraint extends CoreEntityChangedConstraint {
}
