<?php

namespace Drupal\entity_distribution_client\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\EntityChangedConstraintValidator as CoreEntityChangedConstraintValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Class SkipEntityChangedConstraintValidator.
 *
 * @package Drupal\entity_distribution_client\Plugin\Validation\Constraint
 */
class SkipEntityChangedConstraintValidator extends CoreEntityChangedConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    if (!isset($entity) || $entity->isNew()) {
      parent::validate($entity, $constraint);
    }

    $is_inline_entity = \Drupal::service('entity_distribution_client.entity_helper')->isInlineEntity($entity);
    if (!$is_inline_entity) {
      parent::validate($entity, $constraint);
    }
  }

}
