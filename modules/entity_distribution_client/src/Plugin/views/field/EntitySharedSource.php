<?php

namespace Drupal\entity_distribution_client\Plugin\views\field;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\entity_share_client\Entity\Remote;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntitySharedSource.
 *
 * @package Drupal\entity_distribution_client\Plugin\views\field
 *
 * @ViewsField("entity_shared_source")
 */
class EntitySharedSource extends FieldPluginBase {

  /**
   * The state information.
   *
   * @var \Drupal\entity_share_client\Service\StateInformationInterface
   */
  protected $stateInformation;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->stateInformation = $container->get('entity_share_client.state_information');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    if (!($entity instanceof ContentEntityInterface)) {
      return '';
    }
    $import_status = $this->stateInformation->getImportStatusOfEntity($entity);
    if (empty($import_status)) {
      return '';
    }
    $langcode = $entity->language()->getId();
    $remote_website = $import_status->get('remote_website')->value;
    $remote = Remote::load($remote_website);
    if ($remote instanceof TranslatableInterface) {
      $remote = $remote->hasTranslation($langcode) ? $remote->getTranslation($langcode) : $remote;
    }
    return $remote ? $remote->label() : '';
  }

}
