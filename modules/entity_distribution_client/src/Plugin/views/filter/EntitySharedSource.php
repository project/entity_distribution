<?php

namespace Drupal\entity_distribution_client\Plugin\views\filter;

use Drupal\Core\Cache\Cache;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filters by node source filter.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("entity_shared_source")
 */
class EntitySharedSource extends InOperator {

  const SOURCE_LOCAL = '_local';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The remote storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $remoteStorage;

  /**
   * The remote helper.
   *
   * @var \Drupal\entity_distribution\Helper\RemoteHelperInterface
   */
  protected $remoteHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->remoteStorage = $instance->entityTypeManager->getStorage('remote');
    $instance->remoteHelper = $container->get('entity_distribution.remote_helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->entityTypeId = $this->getEntityType();
    $this->entityType = \Drupal::entityTypeManager()->getDefinition($this->entityTypeId);
  }

  /**
   * {@inheritdoc}
   */
  public function ensureMyTable() {
    if (!isset($this->tableAlias)) {
      $table_alias = $this->query->ensureTable($this->table, $this->relationship);

      $left_entity_type = $this->entityTypeManager->getDefinition($this->getEntityType());
      $entity_type = $this->entityTypeManager->getDefinition('entity_import_status');
      $configuration = [
        'table' => $entity_type->getBaseTable(),
        'field' => 'entity_id',
        'left_table' => $table_alias,
        'left_field' => $left_entity_type->getKey('id'),
        'extra' => [
          [
            'field' => 'entity_type_id',
            'value' => $left_entity_type->id(),
          ],
        ],
      ];
      if ($left_entity_type->isTranslatable()) {
        $configuration['extra'][] = [
          'field' => $entity_type->getKey('langcode'),
          'left_field' => $left_entity_type->getKey('langcode'),
        ];
      }
      $join = Views::pluginManager('join')->createInstance('standard', $configuration);
      $this->tableAlias = $this->query->addRelationship('entity_import_status', $join, 'entity_import_status');
    }

    return $this->tableAlias;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), $this->remoteStorage->getEntityType()->getListCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), $this->remoteStorage->getEntityType()->getListCacheContexts());
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (!isset($this->valueOptions)) {
      $remotes = $this->remoteHelper->getRemoteWebsites(TRUE);
      $this->valueTitle = $this->t('Remotes');

      $options = [
        // static::SOURCE_LOCAL => 'Local',
      ];
      foreach ($remotes as $remote) {
        $options[$remote->id()] = $remote->label();
      }
      $this->valueOptions = $options;
    }
    return $this->valueOptions;
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple() {
    if (empty($this->value)) {
      return;
    }

    $this->ensureMyTable();

    $operator = $this->operator === 'in' ? '=' : '<>';

    $field = $this->view->query->getConnection()->condition('OR');
    foreach ((array) $this->value as $value) {
      $and = $this->view->query->getConnection()->condition('AND');
      if ($value === static::SOURCE_LOCAL) {
        $and->condition("{$this->tableAlias}.remote_website", NULL, 'IS NULL');
      }
      else {
        $and->condition("{$this->tableAlias}.remote_website", $value, $operator);
      }
      $field->condition($and);
    }

    $this->query->addWhere($this->options['group'], $field);
  }

  /**
   * {@inheritdoc}
   */
  protected function opEmpty() {
    $this->ensureMyTable();
    $operator = $this->operator == 'empty' ? 'IS NULL' : 'IS NOT NULL';
    $this->query->addWhere('AND', "{$this->tableAlias}.remote_website", NULL, $operator);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    foreach (array_keys($this->value) as $remote_id) {
      if ($remote = $this->remoteStorage->load($remote_id)) {
        $dependencies[$remote->getConfigDependencyKey()][] = $remote->getConfigDependencyName();
      }
    }

    return $dependencies;
  }

}
