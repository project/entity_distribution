<?php

namespace Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\entity_share_client\ImportProcessor\ImportProcessorInterface;
use Drupal\entity_share_client\ImportProcessor\ImportProcessorPluginBase;
use Drupal\entity_share_client\RuntimeImportContext;
use Drupal\entity_share_client\Service\StateInformationInterface;
use Drupal\pathauto\PathautoState;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AliasUniquifier.
 *
 * @package Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor
 *
 * @ImportProcessor(
 *   id = "alias_uniquifier",
 *   label = @Translation("Alias Uniquifier"),
 *   description = @Translation("Uniquify path alias for remote data."),
 *   stages = {
 *     "prepare_entity_data" = 0,
 *     "process_entity" = 0,
 *   },
 *   locked = false,
 * )
 */
class AliasUniquifier extends ImportProcessorPluginBase implements ImportProcessorInterface, PluginFormInterface {

  const FREQUENCY_NEW = 'new';

  const FREQUENCY_EVERY = 'every';

  /**
   * The state information.
   *
   * @var \Drupal\entity_share_client\Service\StateInformationInterface
   */
  protected $stateInformation;

  /**
   * The alias repository.
   *
   * @var \Drupal\path_alias\AliasRepositoryInterface
   */
  protected $aliasRepository;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->stateInformation = $container->get('entity_share_client.state_information');
    $instance->aliasRepository = $container->get('path_alias.repository');
    $instance->moduleHandler = $container->get('module_handler');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'frequency' => static::FREQUENCY_NEW,
      'uniquify' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['frequency'] = [
      '#type' => 'radios',
      '#title' => $this->t('How to update alias ?'),
      '#options' => [
        static::FREQUENCY_NEW => $this->t('Update alias only the first time'),
        static::FREQUENCY_EVERY => $this->t('Update alias on every import')
      ],
      '#default_value' => $this->configuration['frequency'],
    ];

    $form['uniquify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Uniquify alias'),
      '#description' => $this->t('Do we need uniquify alias ?'),
      '#default_value' => $this->configuration['uniquify'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareEntityData(RuntimeImportContext $runtime_import_context, array &$entity_json_data) {
    // No do anything if the entity haven't path alias.
    if (!isset($entity_json_data['attributes']['path']['alias'], $entity_json_data['attributes']['path']['langcode'])) {
      return;
    }

    $frequency = $this->configuration['frequency'];
    $uniquify = $this->configuration['uniquify'];

    $original_alias = $entity_json_data['attributes']['path']['alias'];
    $original_langcode = $entity_json_data['attributes']['path']['langcode'];

    $original_uniquify_alias = $uniquify ? $this->aliasUniquify($original_alias, $original_langcode) : $original_alias;
    $original_uniquify_alias = '/' . ltrim($original_uniquify_alias, '/');

    $status_info = $this->stateInformation->getStatusInfo($entity_json_data);

    // If the entity is newly.
    if (in_array($status_info['info_id'], [StateInformationInterface::INFO_ID_NEW, StateInformationInterface::INFO_ID_NEW_TRANSLATION])) {
      // Alias uniquify.
      $entity_json_data['attributes']['path']['alias'] = $original_uniquify_alias;
      // Drop pid. Core must create a new alias.
      unset($entity_json_data['attributes']['path']['pid']);
    }
    // If the entity was existing.
    elseif (($existing_entity = $this->getExistingEntity($entity_json_data)) && !$existing_entity->get('path')->isEmpty()) {
      $existing_alias_value = $existing_entity->get('path')->first()->getValue();

      // Override alias pid to locally.
      $entity_json_data['attributes']['path']['pid'] = $existing_alias_value['pid'];

      // Not update alias to remote version.
      if ($existing_alias_value['alias'] == $original_alias || $frequency === static::FREQUENCY_NEW) {
        $entity_json_data['attributes']['path']['alias'] = $existing_alias_value['alias'];
      }
      // "Every" update alias to remote version.
      elseif ($frequency === static::FREQUENCY_EVERY) {
        $entity_json_data['attributes']['path']['alias'] = $original_uniquify_alias;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processEntity(RuntimeImportContext $runtime_import_context, ContentEntityInterface $processed_entity, array $entity_json_data) {
    // Set pathauto state to skip.
    if ($processed_entity->hasField('path') && $this->moduleHandler->moduleExists('pathauto')) {
      $processed_entity->get('path')->pathauto = PathautoState::SKIP;
    }
  }

  /**
   * Get the existing entity.
   *
   * @param array $entity_json_data
   *   The entity json data.
   *
   * @return \Drupal\Core\Entity\EntityInterface|NULL
   *   The existing entity.
   */
  protected function getExistingEntity(array $entity_json_data) {
    [$entity_type_id, $entity_bundle] = explode('--', $entity_json_data['type']);
    $langcode = $entity_json_data['attributes']['langcode'] ?? '';

    $entities = $this->entityTypeManager->getStorage($entity_type_id)->loadByProperties(['uuid' => $entity_json_data['id']]);
    $entity = reset($entities);

    if ($entity instanceof TranslatableInterface) {
      $entity = $entity->hasTranslation($langcode) ? $entity->getTranslation($langcode) : $entity;
    }

    return $entity;
  }

  /**
   * Simple uniquify.
   *
   * @param string $alias
   *   The alias.
   * @param string $langcode
   *   The langcode.
   *
   * @return string
   *   The uniquified alias.
   */
  protected function aliasUniquify(string $alias, string $langcode) {
    // If the alias already uniquify.
    if (!$this->aliasRepository->lookupByAlias($alias, $langcode)) {
      return $alias;
    }

    // Processing.
    $original_alias = $alias;

    $i = 0;
    while ((bool) $this->aliasRepository->lookupByAlias($alias, $langcode)) {
      $alias = $original_alias . '-' . $i;
      $i++;
    }

    return $alias;
  }

}
