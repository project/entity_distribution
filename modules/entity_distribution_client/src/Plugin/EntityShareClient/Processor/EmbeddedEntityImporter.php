<?php

namespace Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\entity_share\EntityShareUtility;
use Drupal\entity_share_client\Plugin\EntityShareClient\Processor\EmbeddedEntityImporter as CoreEmbeddedEntityImporter;
use Drupal\entity_share_client\RuntimeImportContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EmbeddedEntityImporter.
 *
 * @package Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor
 */
class EmbeddedEntityImporter extends CoreEmbeddedEntityImporter {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Entity import state information service.
   *
   * @var \Drupal\entity_share_client\Service\StateInformationInterface
   */
  protected $stateInformation;

  /**
   * The inline entity helper.
   *
   * @var \Drupal\entity_distribution_client\Helper\EntityHelperInterface
   */
  protected $entityHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance =  parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->stateInformation = $container->get('entity_share_client.state_information');
    $instance->entityHelper = $container->get('entity_distribution_client.entity_helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareImportableEntityData(RuntimeImportContext $runtime_import_context, array &$entity_json_data) {
    parent::prepareImportableEntityData($runtime_import_context, $entity_json_data);

    if (!isset($entity_json_data['attributes']) || !is_array($entity_json_data['attributes'])) {
      return;
    }

    foreach ($entity_json_data['attributes'] as &$field_data) {
      if (!is_array($field_data)) {
        continue;
      }
      if (EntityShareUtility::isNumericArray($field_data)) {
        foreach ($field_data as $delta => $field_value) {
          // Detect formatted text fields.
          if (!isset($field_value['value'], $field_value['format'])) {
            continue;
          }
          $this->massageFormattedText($field_data[$delta]);
        }
      }
      // Detect formatted text fields.
      elseif (isset($field_data['value'], $field_data['format'])) {
        $this->massageFormattedText($field_data);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processEntity(RuntimeImportContext $runtime_import_context, ContentEntityInterface $processed_entity, array $entity_json_data) {
    if ($this->entityHelper->isInlineEntity($processed_entity)) {
      $processed_entity->setNewRevision();
      $processed_entity->isDefaultRevision(TRUE);
    }
  }

  /**
   * Massage formatted text.
   *
   * @param $field_value
   *   The field value.
   */
  protected function massageFormattedText(&$field_value) {
    $text = $field_value['value'];

    if (strpos($text, 'data-entity-type') !== FALSE && (strpos($text, 'data-entity-embed-display') !== FALSE || strpos($text, 'data-view-mode') !== FALSE)) {
      $dom = Html::load($field_value['value']);
      $xpath = new \DOMXPath($dom);

      $changed = FALSE;

      foreach ($xpath->query('//drupal-entity[@data-entity-type and (@data-entity-uuid or @data-entity-id or @data-entity-revision) and (@data-entity-embed-display or @data-view-mode)]') as $node) {
        /** @var \DOMElement $node */
        $entity_type = $node->getAttribute('data-entity-type');
        $uuid = $node->getAttribute('data-entity-uuid');

        $import_status = $this->stateInformation->getImportStatusByParameters($uuid, $entity_type);

        // The entity does not import, skip.
        if (empty($import_status)) {
          $node->removeAttribute('data-entity-id');
          $node->removeAttribute('data-entity-revision');
          $changed = TRUE;
          continue;
        }
        // Try get entity.
        else {
          $entity = $this->entityTypeManager->getStorage($entity_type)->loadByProperties(['uuid' => $uuid]);
          $entity = current($entity);
        }

        // Missing entity with uuid, skip.
        if (empty($entity) || !$entity instanceof EntityInterface) {
          continue;
        }

        $node->setAttribute('data-entity-id', $entity->id());

        if ($import_status->hasField('entity_revision')) {
          // If the entity latest revision same with the latest import revision, update.
          if ($entity instanceof RevisionableInterface && $entity->getRevisionId() === $import_status->get('entity_revision')->value) {
            $node->setAttribute('data-entity-revision', $entity->getRevisionId());
          }
        }

        $changed = TRUE;
      }

      if ($changed) {
        $field_value['value'] = Html::serialize($dom);
      }
    }
  }

}
