<?php

namespace Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\entity_share_client\ImportProcessor\ImportProcessorInterface;
use Drupal\entity_share_client\ImportProcessor\ImportProcessorPluginBase;
use Drupal\entity_share_client\RuntimeImportContext;
use Drupal\workflows\Entity\Workflow;
use Drupal\workflows\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ModeratedEntity.
 *
 * @package Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor
 *
 * @ImportProcessor(
 *   id = "moderated_entity_processor",
 *   label = @Translation("Moderated entity"),
 *   description = @Translation("Handle the moderated entity before save."),
 *   stages = {
 *     "prepare_entity_data" = 0,
 *     "process_entity" = 0,
 *   },
 *   locked = false,
 * )
 */
class ModeratedEntity extends ImportProcessorPluginBase implements ImportProcessorInterface, PluginFormInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The moderation information.
   *
   * @var \Drupal\content_moderation\ModerationInformationInterface
   */
  protected $moderationInformation;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->moderationInformation = $container->get('content_moderation.moderation_information', ContainerInterface::NULL_ON_INVALID_REFERENCE);
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'import_moderation_state' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    if (!$this->moduleHandler->moduleExists('content_moderation')) {
      $form['messages'] = [
        '#theme' => 'status_messages',
        '#message_list' => ['warning' => [$this->t('The Content Moderation module was disabled.')]],
        '#status_headings' => [
          'warning' => t('Warning message'),
        ],
      ];
      return $form;
    }

    $workflows = Workflow::loadMultiple();

    $form['import_moderation_state'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Import Moderation State'),
    ];

    foreach ($workflows as $workflow) {
      $form['import_moderation_state'][$workflow->id()] = [
        '#type' => 'select',
        '#title' => $this->t('Workflow %workflow: ', ['%workflow' => $workflow->label()]),
        '#options' => ['' => $this->t('- None -')] + array_map(function (StateInterface $state) {
          return $state->label();
        }, $workflow->getTypePlugin()->getStates()),
        '#default_value' => $this->configuration['import_moderation_state'][$workflow->id()] ?? '',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareEntityData(RuntimeImportContext $runtime_import_context, array &$entity_json_data) {
    if (!$this->moduleHandler->moduleExists('content_moderation')) {
      return;
    }

    [$entity_type_id, $entity_bundle] = explode('--', $entity_json_data['type']);

    $workflow = $this->moderationInformation->getWorkflowForEntityTypeAndBundle($entity_type_id, $entity_bundle);

    if (!$workflow || empty($this->configuration['import_moderation_state'][$workflow->id()])) {
      return;
    }

    $import_moderation_state = $this->configuration['import_moderation_state'][$workflow->id()];
    $entity_json_data['attributes']['moderation_state'] = $import_moderation_state;
  }

  /**
   * {@inheritdoc}
   */
  public function processEntity(RuntimeImportContext $runtime_import_context, ContentEntityInterface $processed_entity, array $entity_json_data) {
    if (!$this->moduleHandler->moduleExists('content_moderation')) {
      return;
    }

    if ($processed_entity->isNew() || !$this->moderationInformation->isModeratedEntity($processed_entity)) {
      return;
    }

    // Put the original to populate entity.
    $storage = $this->entityTypeManager->getStorage($processed_entity->getEntityTypeId());
    $latest_translation_affected_revision_ids = [];
    foreach ($processed_entity->getTranslationLanguages() as $language) {
      $latest_translation_affected_revision_ids[] = $storage->getLatestTranslationAffectedRevisionId($processed_entity->id(), $language->getId());
    }
    if (count($latest_translation_affected_revision_ids) > 0) {
      $latest_translation_affected_revision = $storage->loadRevision(max($latest_translation_affected_revision_ids));
      $processed_entity->original = $latest_translation_affected_revision;
    }

    // Calc default revision.
    $workflow = $this->moderationInformation->getWorkflowForEntity($processed_entity);
    $current_state = $workflow->getTypePlugin()->getState($processed_entity->moderation_state->value);
    $processed_entity->setSyncing(TRUE);
    $processed_entity->setNewRevision();
    $processed_entity->isDefaultRevision($current_state->isDefaultRevisionState());
  }

}
