<?php

namespace Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_share_client\ImportProcessor\ImportProcessorInterface;
use Drupal\entity_share_client\Plugin\EntityShareClient\Processor\DefaultDataProcessor as CoreDefaultDataProcessor;
use Drupal\entity_share_client\RuntimeImportContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DefaultDataProcessor.
 *
 * @package Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor
 */
class DefaultDataProcessor extends CoreDefaultDataProcessor implements ImportProcessorInterface {

  /**
   * The resource types.
   *
   * @var array
   */
  protected $resourceTypes = [];

  /**
   * The entity type definition.
   *
   * @var array
   */
  protected $entityTypeDefinition = [];

  /**
   * The operation policy helper.
   *
   * @var \Drupal\entity_distribution_client\Helper\OpPolicyHelperInterface
   */
  protected $opPolicyHelper;

  /**
   * The resource type repository.
   *
   * @var \Drupal\jsonapi\ResourceType\ResourceTypeRepositoryInterface
   */
  protected $resourceTypeRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance =  parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->opPolicyHelper = $container->get('entity_distribution_client.op_policy_helper');
    $instance->resourceTypeRepository = $container->get('jsonapi.resource_type.repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'update_policy' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['update_policy']['#disabled'] = TRUE;
    $form['update_policy']['#description'] .= $this->t('<br><strong>Entity Distribution will force update import policy.</strong>');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareImportableEntityData(RuntimeImportContext $runtime_import_context, array &$entity_json_data) {
    parent::prepareImportableEntityData($runtime_import_context, $entity_json_data);

    // Tempstore the entity json data.
    $uuid = $entity_json_data['id'];
    $langcode = $entity_json_data['attributes']['langcode'] ?? '';
    $runtime_import_context->tmpEntityJsonData[$uuid][$langcode] = $entity_json_data;
  }

  /**
   * {@inheritdoc}
   */
  public function postEntitySave(RuntimeImportContext $runtime_import_context, ContentEntityInterface $processed_entity) {
    parent::postEntitySave($runtime_import_context, $processed_entity);

    $uuid = $processed_entity->uuid();
    $langcode = $processed_entity->language()->getId();
    $entity_type_id = $processed_entity->getEntityTypeId();
    $entity_bundle = $processed_entity->bundle();
    $resource_type = $this->getResourceType($entity_type_id, $entity_bundle);
    $entity_json_data = $runtime_import_context->tmpEntityJsonData[$uuid][$langcode] ?? [];

    // Set operation policy.
    $op_policy = $this->opPolicyHelper->getOpPolicy($processed_entity, $this->configuration['policy']);

    if ($op_policy) {
      $languages = $processed_entity->getTranslationLanguages();

      // Set new policy for all translations.
      foreach ($languages as $langcode => $language) {
        $translation = $processed_entity->getTranslation($langcode);
        $import_status_entity = $this->stateInformation->getImportStatusOfEntity($translation);
        if (!$import_status_entity) {
          continue;
        }
        $import_status_entity->setPolicy($op_policy)->save();
      }
    }

    // Save the remote attributes.
    $import_status_entity = $this->stateInformation->getImportStatusOfEntity($processed_entity);
    if ($entity_json_data && $import_status_entity && $import_status_entity->hasField('remote_attributes')) {
      $remote_attributes = $import_status_entity->get('remote_attributes')->isEmpty() ? serialize([]) : $import_status_entity->get('remote_attributes')->value;
      $remote_attributes = unserialize($remote_attributes);

      $entity_type = $processed_entity->getEntityType();

      // Remote id.
      $id_public_name = $resource_type->getPublicName($entity_type->getKey('id'));
      if (isset($entity_json_data['attributes'][$id_public_name])) {
        $remote_attributes['id'] = $entity_json_data['attributes'][$id_public_name];
      }

      // Remote url.
      if (isset($entity_json_data['links']['canonical']['href'])) {
        $remote_attributes['canonical'] = $entity_json_data['links']['canonical']['href'];
      }

      $import_status_entity->set('remote_attributes', serialize($remote_attributes))->save();
    }
  }

  /**
   * Gets a specific JSON:API resource type based on entity type ID and bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle
   *   The ID for the bundle to find. If the entity type does not have a bundle,
   *   then the entity type ID again.
   *
   * @return \Drupal\jsonapi\ResourceType\ResourceType
   *   The requested JSON:API resource type, if it exists. NULL otherwise.
   *
   * @see \Drupal\Core\Entity\EntityInterface::bundle()
   */
  protected function getResourceType($entity_type_id, $bundle) {
    if (!isset($this->resourceTypes[$entity_type_id][$bundle])) {
      $this->resourceTypes[$entity_type_id][$bundle] = $this->resourceTypeRepository->get($entity_type_id, $bundle);
    }
    return $this->resourceTypes[$entity_type_id][$bundle];
  }

}
