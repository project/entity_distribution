<?php

namespace Drupal\entity_distribution_client\Helper;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\entity_share_client\Entity\ImportConfigInterface;
use Drupal\entity_share_client\Entity\Remote;
use Drupal\entity_share_client\Service\StateInformationInterface;

/**
 * Class FormHelper.
 *
 * @package Drupal\entity_distribution_client\Helper
 */
class FormHelper {

  use StringTranslationTrait;

  /**
   * The import config helper.
   *
   * @var \Drupal\entity_distribution_client\Helper\ImportConfigHelperInterface
   */
  protected $importConfigHelper;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The state information.
   *
   * @var \Drupal\entity_share_client\Service\StateInformationInterface
   */
  protected $stateInformation;

  /**
   * The policy manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $policyManager;

  /**
   * FormHelper constructor.
   *
   * @param \Drupal\entity_distribution_client\Helper\ImportConfigHelperInterface $import_config_helper
   *   The import config helper.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\entity_share_client\Service\StateInformationInterface $state_information
   *   The state information.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $policy_manager
   *   The policy manager.
   */
  public function __construct(ImportConfigHelperInterface $import_config_helper, AccountProxyInterface $current_user, StateInformationInterface $state_information, PluginManagerInterface $policy_manager) {
    $this->importConfigHelper = $import_config_helper;
    $this->currentUser = $current_user;
    $this->stateInformation = $state_information;
    $this->policyManager = $policy_manager;
  }

  /**
   * Alter the entity form element.
   *
   * @param array $element
   *   The element.
   * @param array $values
   *   The values.
   * @param array $context
   *   The context.
   *
   * @return array
   */
  public function alterEntityFormElement(array &$element, array $values, array $context) {
    $entity = $context['items']->getEntity();
    $default_translation = $entity->getTranslation(LanguageInterface::LANGCODE_DEFAULT);
    $default_import_status = $this->stateInformation->getImportStatusOfEntity($default_translation);

    $element['entity_distribution_client'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Client'),
    ];

    if (!$default_import_status) {
      $element['entity_distribution_client']['#description'] = $this->t('The entity is locally.');
    }
    else {
      $policy_plugin = $this->policyManager->getDefinition($default_import_status->getPolicy(), FALSE);
      $element['entity_distribution_client']['#description'] = $this->t(
        'State Information (Original language): <li>Remote: %remote</li><li>Import policy: %import_policy</li>',
        [
          '%remote' => Remote::load($default_import_status->get('remote_website')->value)->label(),
          '%import_policy' => $policy_plugin['label'] ?? $this->t('Unknown'),
        ]
      );
    }

    return $element;
  }

  /**
   * Alter entity type form element.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $type
   *   The type.
   * @param array $context
   *   The context.
   *
   * @return array
   */
  public function alterEntityTypeFormElement(array &$element, ConfigEntityInterface $type, array $context) {
    $import_configs = $this->importConfigHelper->getImportConfigs();

    // Message for import configs.
    if (empty($import_configs)) {
      $element['entity_distribution']['messages']['#message_list']['error'][] = t('Click <a href=":import-config-add-form" target="_blank">here</a> to create default import config for this type.', [
        ':import-config-add-form' => Url::fromRoute('entity.import_config.add_form')->toString(),
      ]);
    }

    // Default import config.
    $element['entity_distribution']['default_import_config'] = [
      '#type' => 'select',
      '#title' => t('Default import config'),
      '#options' => array_map(function (ImportConfigInterface $import_config) {
        return $import_config->label();
      }, $import_configs),
      '#default_value' => $type->getThirdPartySetting('entity_distribution', 'default_import_config'),
      '#parents' => ['entity_distribution', 'default_import_config'],
      '#states' => [
        'required' => [
          ':input[name="entity_distribution[enable]"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name="entity_distribution[enable]"]' => ['checked' => TRUE],
        ],
      ],
      '#element_validate' => [[static::class, 'validateDefaultImportConfig']],
    ];

    if ($this->currentUser->hasPermission('administer_import_config_entity')) {
      $element['entity_distribution']['default_import_config']['#description'] = t('Click <a href=":import-config-collection" target="_blank">here</a> to administer import config entity.', [
        ':import-config-collection' => Url::fromRoute('entity.import_config.collection')->toString(),
      ]);
    }

    // Libraries.
    $element['entity_distribution']['#attached']['library'][] = 'entity_distribution_client/fieldset_summaries';

    $element['#entity_builders'][] = [static::class, 'entityTypeFormBuilder'];

    return $element;
  }

  /**
   * Validate the default import config.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateDefaultImportConfig(array $element, FormStateInterface $form_state) {
    if (!$form_state->getValue(['entity_distribution', 'enable'])) {
      return;
    }

    if (empty($element['#value']) || $element['#value'] == '_none') {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
    }
  }

  /**
   * The entity type form builder.
   *
   * @param $entity_type
   *   The entity type.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $type
   *   The type.
   * @param $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function entityTypeFormBuilder($entity_type_id, ConfigEntityInterface $type, &$form, FormStateInterface $form_state) {
    // Save config into third party setting.
    $default_import_config = $form_state->getValue(['entity_distribution', 'default_import_config'], '');
    $type->setThirdPartySetting('entity_distribution', 'default_import_config', $default_import_config);
  }

}
