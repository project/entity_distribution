<?php

namespace Drupal\entity_distribution_client\Helper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\entity_share_client\Service\StateInformationInterface;

/**
 * Class EntityHelper.
 *
 * @package Drupal\entity_distribution_client\Helper
 */
class EntityHelper implements EntityHelperInterface {

  const INLINE_ENTITY_TYPES = ['block_content', 'paragraph'];

  /**
   * The state information.
   *
   * @var \Drupal\entity_share_client\Service\StateInformationInterface
   */
  protected $stateInformation;

  /**
   * EntityHelper constructor.
   *
   * @param \Drupal\entity_share_client\Service\StateInformationInterface $state_information
   *   The state information.
   */
  public function __construct(StateInformationInterface $state_information) {
    $this->stateInformation = $state_information;
  }

  /**
   * {@inheritdoc}
   */
  public function isInlineEntity(EntityInterface $entity) {
    $entity_type_id = $entity->getEntityTypeId();

    if (!in_array($entity_type_id, static::INLINE_ENTITY_TYPES)) {
      return FALSE;
    }

    /** @var \Drupal\block_content\BlockContentInterface $entity */
    if ($entity_type_id === 'block_content' && $entity->isReusable()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function comeFromSharing(ContentEntityInterface $entity, bool $return_object = FALSE) {
    $default_translation = $entity->getTranslation(LanguageInterface::LANGCODE_DEFAULT);
    $import_status = $this->stateInformation->getImportStatusOfEntity($default_translation);
    return $return_object ? $import_status : !empty($import_status);
  }

}
