<?php

namespace Drupal\entity_distribution_client\Helper;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_share_client\Entity\EntityImportStatusInterface;
use Drupal\entity_share_client\Service\StateInformationInterface;

/**
 * Class OpPolicyHelper.
 *
 * @package Drupal\entity_distribution_client\Helper
 */
class OpPolicyHelper implements OpPolicyHelperInterface {

  use StringTranslationTrait;

  const POLICY_LOCKED = 'distribution_locked';

  const POLICY_ONLY_EDIT = 'distribution_only_edit';

  const POLICY_ONLY_TRANSLATE = 'distribution_only_translate';

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The state information.
   *
   * @var \Drupal\entity_share_client\Service\StateInformationInterface
   */
  protected $stateInformation;

  /**
   * OpPolicyHelper constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\entity_share_client\Service\StateInformationInterface $state_information
   *   The state information.
   */
  public function __construct(MessengerInterface $messenger, StateInformationInterface $state_information) {
    $this->messenger = $messenger;
    $this->stateInformation = $state_information;
  }

  /**
   * {@inheritdoc}
   */
  public function getOpPolicy(FieldableEntityInterface $entity, string $default_policy = EntityImportStatusInterface::IMPORT_POLICY_DEFAULT) {
    if (!$entity->hasField('entity_distribution') || $entity->get('entity_distribution')->isEmpty()) {
      return NULL;
    }

    $entity_distribution = $entity->get('entity_distribution')->first()->getValue();

    $allow_edit = $entity_distribution['allow_edit'] ?? TRUE;
    $allow_translate = $entity_distribution['allow_translate'] ?? TRUE;

    $policy = $default_policy;

    if ($allow_edit && !$allow_translate) {
      $policy = static::POLICY_ONLY_EDIT;
    }
    elseif (!$allow_edit && $allow_translate) {
      $policy = static::POLICY_ONLY_TRANSLATE;
    }
    elseif (!$allow_edit && !$allow_translate) {
      $policy = static::POLICY_LOCKED;
    }

    return $policy;
  }

  /**
   * {@inheritdoc}
   */
  public function formAlter(array &$form, FormStateInterface $form_state, $form_id) {
    $form_object = $form_state->getFormObject();

    // Check if acting on an entity form.
    if (!$form_object instanceof EntityFormInterface) {
      return;
    }

    $form_storage = $form_state->getStorage();

    if (!preg_match('/_edit_form$/', $form_id) && !isset($form_storage['content_translation'])) {
      return;
    }

    $entity = $form_object->getEntity();
    $entity_type = $entity->getEntityType();
    $entity_type_id = $entity_type->id();

    // Check if it is a content entity.
    if ($entity_type->getGroup() != 'content') {
      return;
    }

    // Do not act on user.
    if ($entity_type_id == 'user') {
      return;
    }

    // If the entity type does not have a UUID it can not be imported with
    // Entity Share.
    if (!$entity_type->hasKey('uuid')) {
      return;
    }

    $default_translation = $entity->getTranslation(LanguageInterface::LANGCODE_DEFAULT);

    $import_status = $this->stateInformation->getImportStatusOfEntity($default_translation);

    // Check if the original entity is from an import.
    if (!$import_status) {
      return;
    }

    $policy = $import_status->getPolicy();
    $operate = $default_translation->language()->getId() != $entity->language()->getId() ? 'translate' : 'edit';
    $operate_message = '';

    if ($policy == static::POLICY_LOCKED) {
      $form['#disabled'] = TRUE;
      $operate_message = $this->t('you cannot edit or translate');
    }
    elseif ($policy == static::POLICY_ONLY_TRANSLATE && $operate != 'translate') {
      $form['#disabled'] = TRUE;
      $operate_message = $this->t('you can only translate');
    }
    elseif ($policy == static::POLICY_ONLY_EDIT && $operate != 'edit') {
      $form['#disabled'] = TRUE;
      $operate_message = $this->t('you can only edit original language');
    }

    // Show message.
    if ($operate_message) {
      $this->messenger->addWarning($this->t('The entity had been locked from edition because of an import policy, @operate_message.', [
        '@operate_message' => $operate_message,
      ]));
    }
  }

}
