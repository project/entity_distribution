<?php

namespace Drupal\entity_distribution_client\Helper;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface EntityHelperInterface.
 *
 * @package Drupal\entity_distribution_client\Helper
 */
interface EntityHelperInterface {

  /**
   * Is inline entity ?
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   */
  public function isInlineEntity(EntityInterface $entity);

  /**
   * Came from sharing ?
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param bool $return_object
   *   Return object.
   *
   * @return bool|\Drupal\entity_share_client\Entity\EntityImportStatusInterface
   */
  public function comeFromSharing(ContentEntityInterface $entity, bool $return_object = FALSE);

}
