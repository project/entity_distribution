<?php

namespace Drupal\entity_distribution_client\Helper;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_share_client\Entity\ImportConfig;

/**
 * Class ImportConfigHelper.
 *
 * @package Drupal\entity_distribution_client\Helper
 */
class ImportConfigHelper implements ImportConfigHelperInterface {

  use StringTranslationTrait;

  /**
   * The default import config.
   *
   * @var array
   */
  protected $defaultImportConfig = [];

  /**
   * The import configs.
   *
   * @var array
   */
  protected $importConfigs = [];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ChannelHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultImportConfig(ConfigEntityInterface $entity, $throw = FALSE) {
    if (!isset($this->defaultImportConfig[$entity->id()])) {
      $this->defaultImportConfig[$entity->id()] = $entity->getThirdPartySetting('entity_distribution', 'default_import_config') ?: 0;
      $this->defaultImportConfig[$entity->id()] = ImportConfig::load($this->defaultImportConfig[$entity->id()]);
    }

    if ($throw && empty($this->defaultImportConfig[$entity->id()])) {
      throw new \Exception($this->t('Missing default import config on @entity_type:@entity_bundle.', [
        '@entity_type' => $entity->getEntityType()->getBundleOf(),
        '@entity_bundle' => $entity->id(),
      ]));
    }

    return $this->defaultImportConfig[$entity->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getImportConfigs() {
    if (!$this->importConfigs) {
      $this->importConfigs = $this->entityTypeManager->getStorage('import_config')->loadMultiple();
    }
    return $this->importConfigs;
  }

}
