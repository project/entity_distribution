<?php

namespace Drupal\entity_distribution_client\Helper;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_share_client\Entity\EntityImportStatusInterface;

/**
 * Interface OpPolicyHelperInterface.
 *
 * @package Drupal\entity_distribution_client\Helper
 */
interface OpPolicyHelperInterface {

  /**
   * Get the operation policy.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The content entity.
   * @param string $default_policy
   *   The default policy.
   *
   * @return string
   */
  public function getOpPolicy(FieldableEntityInterface $entity, string $default_policy = EntityImportStatusInterface::IMPORT_POLICY_DEFAULT);

  /**
   * Apply the entity policy.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form. The arguments that
   *   \Drupal::formBuilder()->getForm() was originally called with are
   *   available in the array $form_state->getBuildInfo()['args'].
   * @param string $form_id
   *   String representing the name of the form itself. Typically, this is the
   *   name of the function that generated the form.
   */
  public function formAlter(array &$form, FormStateInterface $form_state, $form_id);

}
