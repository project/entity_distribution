<?php

namespace Drupal\entity_distribution_client\Helper;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface ImportConfigHelperInterface.
 *
 * @package Drupal\entity_distribution_client\Helper
 */
interface ImportConfigHelperInterface {

  /**
   * Get the default import config.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\entity_share_client\Entity\ImportConfigInterface|NULL
   *   The import config.
   */
  public function getDefaultImportConfig(ConfigEntityInterface $entity);

  /**
   * Get the import configs.
   *
   * @return array
   */
  public function getImportConfigs();

}
