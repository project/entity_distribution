<?php

namespace Drupal\entity_distribution_client\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_share_client\Entity\Remote;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class Notify.
 *
 * @package Drupal\entity_distribution_client\Controller
 */
class Notify extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The import config helper.
   *
   * @var \Drupal\entity_distribution_client\Helper\ImportConfigHelperInterface
   */
  protected $importConfigHelper;

  /**
   * The remote manager.
   *
   * @var \Drupal\entity_share_client\Service\RemoteManagerInterface
   */
  protected $remoteManager;

  /**
   * The queue helper.
   *
   * @var \Drupal\entity_share_async\Service\QueueHelperInterface
   */
  protected $queueHelper;

  /**
   * Get the import config helper.
   *
   * @return \Drupal\entity_distribution_client\Helper\ImportConfigHelperInterface
   */
  protected function importConfigHelper() {
    if (!$this->importConfigHelper) {
      $this->importConfigHelper = \Drupal::service('entity_distribution_client.import_config_helper');
    }
    return $this->importConfigHelper;
  }

  /**
   * Get the remote manager.
   *
   * @return \Drupal\entity_share_client\Service\RemoteManagerInterface
   */
  protected function remoteManager() {
    if (!$this->remoteManager) {
      $this->remoteManager = \Drupal::service('entity_share_client.remote_manager');
    }
    return $this->remoteManager;
  }

  /**
   * The queue helper.
   *
   * @return \Drupal\entity_share_async\Service\QueueHelperInterface
   */
  protected function queueHelper() {
    if (!$this->queueHelper) {
      $this->queueHelper = \Drupal::service('entity_share_async.queue_helper');
    }
    return $this->queueHelper;
  }

  /**
   * Notify.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function index(Request $request) {
    $data = Json::decode($request->getContent());

    if (!isset($data['source'], $data['channel'], $data['uuids'], $data['langcode'])) {
      throw new HttpException(400, 'Some required fields was missing: source, channel, uuids, langcode.');
    }

    $remote_id = $data['source'];
    $remote = Remote::load($remote_id);

    // Remote.
    if (!$remote_id) {
      throw new HttpException(400, 'The source website was not register on remote website.');
    }

    // Channel.
    $channel_id = $data['channel'];
    $channel_infos = $this->remoteManager()->getChannelsInfos($remote);
    $channel_info = $channel_infos[$channel_id] ?? [];

    if (!isset($channel_info['channel_entity_type'], $channel_info['channel_bundle'])) {
      throw new HttpException(400, $this->t('Missing channel (@channel) on source website.', [
        '@channel' => $channel_id,
      ]));
    }

    try {
      $entity_type = $this->entityTypeManager()->getDefinition($channel_info['channel_entity_type']);
      $bundle_entity_type_id = $entity_type->getBundleEntityType();
      $bundle_entity_type = $this->entityTypeManager()->getStorage($bundle_entity_type_id)->load($channel_info['channel_bundle']);

      if (!$bundle_entity_type) {
        throw new \Exception($this->t('Missing entity type (@entity_type:@entity_bundle) on remote website.', [
          '@entity_type' => $channel_info['channel_entity_type'],
          '@entity_bundle' => $channel_info['channel_bundle'],
        ]));
      }

      // Entity distribution was enabled.
      if (!$bundle_entity_type->getThirdPartySetting('entity_distribution', 'enable')) {
        throw new \Exception($this->t('The Entity Distribution was disabled for entity type (@entity_type:@entity_bundle) on remote website.', [
          '@entity_type' => $channel_info['channel_entity_type'],
          '@entity_bundle' => $channel_info['channel_bundle'],
        ]));
      }

      // Import config.
      $import_config_id = $this->importConfigHelper()->getDefaultImportConfig($bundle_entity_type, TRUE)->id();

      // Langcode.
      $langcode = $data['langcode'];

      if (empty($this->languageManager()->getLanguage($langcode))) {
        throw new \Exception($this->t('Missing language (@langcode) on remote website.', [
          '@langcode' => $langcode,
        ]));
      }

      // Use queue to import entities if the entity_share_async was enabled.
      $this->queueHelper()->enqueue($remote_id, $channel_id, $import_config_id, $data['uuids'], $langcode);
    }
    catch (\Exception $e) {
      throw new HttpException(500, $e->getMessage());
    }

    return new JsonResponse([]);
  }

}
