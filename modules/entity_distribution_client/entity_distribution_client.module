<?php

/**
 * @file
 * Primary module hooks for entity_distribution_client module.
 */

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\entity_distribution_client\Helper\EntityHelper;
use Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor\EmbeddedEntityImporter;
use Drupal\entity_distribution_client\Plugin\EntityShareClient\Processor\DefaultDataProcessor;

/**
 * Implements hook_entity_type_entity_dist_schema_alter().
 */
function entity_distribution_client_entity_type_entity_dist_schema_alter($entity_type, &$schema) {
  if (isset($schema['mapping'])) {
    $schema['mapping']['default_import_config'] = [
      'type' => 'string',
      'label' => 'Default import config to pull from target site.',
    ];
  }
}

/**
 * Implements hook_entity_dist_entity_form_element_alter().
 */
function entity_distribution_client_entity_dist_entity_form_element_alter(&$element, $values, $context) {
  $element += \Drupal::service('entity_distribution_client.form_helper')->alterEntityFormElement($element, $values, $context);
}

/**
 * Implements hook_entity_dist_entity_type_form_element_alter().
 */
function entity_distribution_client_entity_dist_entity_type_form_element_alter(&$element, ConfigEntityInterface $type, $context) {
  $element += \Drupal::service('entity_distribution_client.form_helper')->alterEntityTypeFormElement($element, $type, $context);
}

/**
 * Implements hook_views_data_alter().
 */
function entity_distribution_client_views_data_alter(array &$data) {
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    // Keep only content entity type without user.
    // @see \Drupal\entity_share_server\Form\ChannelForm::getEntityTypeOptions().
    if ($entity_type->getGroup() != 'content' || $entity_type_id == 'user') {
      continue;
    }
    // Keep only content entity type with UUID (required for JSON API).
    if (!$entity_type->hasKey('uuid')) {
      continue;
    }

    $data_table = $entity_type->getDataTable() ?: $entity_type->getBaseTable();

    if (!isset($data[$data_table])) {
      continue;
    }

    $data[$data_table]['entity_shared_source'] = [
      'title' => t('Entity Shared Source'),
      'help' => t('Where the entity from ?'),
      'field' => [
        'id' => 'entity_shared_source',
      ],
      'filter' => [
        'id' => 'entity_shared_source',
        'allow empty' => TRUE
      ],
    ];
  }
}

/**
 * Implements hook_entity_type_alter().
 */
function entity_distribution_client_entity_type_alter(array &$entity_types) {
  foreach (EntityHelper::INLINE_ENTITY_TYPES as $entity_type_id) {
    if (!isset($entity_types[$entity_type_id])) {
      continue;
    }
    $constraint = array_diff_key($entity_types[$entity_type_id]->getConstraints(), ['EntityChanged' => NULL]);
    $constraint['SkipEntityChanged'] = NULL;
    $entity_types[$entity_type_id]->setConstraints($constraint);
  }
}

/**
 * Implements hook_entity_share_client_import_processor_info_alter().
 */
function entity_distribution_client_entity_share_client_import_processor_info_alter(&$definitions) {
  if (isset($definitions['embedded_entity_importer'])) {
    $definitions['embedded_entity_importer']['class'] = EmbeddedEntityImporter::class;
    $definitions['embedded_entity_importer']['stages']['process_entity'] = 0;
  }
  if (isset($definitions['default_data_processor'])) {
    $definitions['default_data_processor']['class'] = DefaultDataProcessor::class;
  }
}

/**
 * Implements hook_form_alter().
 */
function entity_distribution_client_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  return \Drupal::service('entity_distribution_client.op_policy_helper')->formAlter($form, $form_state, $form_id);
}

/**
 * Implements hook_entity_distribution_can_notify().
 */
function entity_distribution_client_entity_distribution_can_notify(ContentEntityInterface $entity) {
  // Prevent secondary share if the original translation came from sharing.
  // @case site1 -> site2 -> site3 -> site1, site recursion.
  // @case site1(en) -> site2(en) <trans to> site2(jp) -> site3(jp) <trans to> site3(en) -> site1(en)
  if (\Drupal::service('entity_distribution_client.entity_helper')->comeFromSharing($entity)) {
    return FALSE;
  }
}

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 */
function entity_distribution_client_field_widget_entity_distribution_form_alter(&$element, FormStateInterface $form_state, $context) {
  if (isset($element['entity_distribution_server'])) {
    $entity = $form_state->getFormObject()->getEntity();
    if (\Drupal::service('entity_distribution_client.entity_helper')->comeFromSharing($entity)) {
      foreach (Element::children($element['entity_distribution_server']) as $field_name) {
        $element['entity_distribution_server'][$field_name]['#access'] = FALSE;
      }
      $element['entity_distribution_server']['#description'] = t('Prevent secondary share, because the original language came from sharing.');
    }
  }
}

/**
 * Implements hook_entity_base_field_info().
 */
function entity_distribution_client_entity_base_field_info(EntityTypeInterface $entity_type) {
  if ($entity_type->id() === 'entity_import_status') {
    $fields['remote_attributes'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Remote attributes'))
      ->setDescription(t('The attributes of imported entity on Client.'));

    return $fields;
  }
}
