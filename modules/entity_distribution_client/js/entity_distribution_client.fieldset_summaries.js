/**
 * @file
 * Attaches entity_distribution behaviors to the entity form.
 */
(function ($, Drupal) {

  "use strict";

  Drupal.behaviors.entityDistFieldsetSummaries.summaryCallbacks.push(function (context, summary) {
    // Remove client summary to regenerate.
    const $original_summary = $('<div>' + summary + '</div>');
    $original_summary.find('span.entity-dist-client-summary').remove();

    const enabled = $(context).find('input:checkbox[name$="[enable]"]').prop('checked');
    if (!enabled) {
      return $original_summary.html();
    }

    // Regenerate client summary.
    summary = '';

    const $default_import_config = $(context).find('select[name$="[default_import_config]"]');
    const val = $default_import_config.val();

    if (val === '_none' || !val) {
      summary += Drupal.t('Missing default import config');
    }
    else {
      summary += Drupal.t('Default import config: %default_import_config', {
        '%default_import_config': $default_import_config.find(':selected').text(),
      })
    }

    return $original_summary.html() + '<span class="entity-dist-client-summary">' + summary + '</span>';
  });

})(jQuery, Drupal);
