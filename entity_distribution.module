<?php

/**
 * @file
 * Primary module hooks for entity_distribution module.
 */

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\jsonapi\JsonApiResource\Link;
use Drupal\jsonapi\JsonApiResource\LinkCollection;

/**
 * Implements hook_config_schema_info_alter().
 */
function entity_distribution_config_schema_info_alter(&$definitions) {
  $module_handler = \Drupal::moduleHandler();
  $form_helper = \Drupal::service('entity_distribution.form_helper');
  $entity_types = $form_helper->getSupportedEntityTypes();

  foreach ($entity_types as $entity_type) {
    $schema = $definitions["{$entity_type}.type.*.third_party.entity_distribution"];
    $module_handler->alter('entity_type_entity_dist_schema', $entity_type, $schema);
    $definitions["{$entity_type}.type.*.third_party.entity_distribution"] = $schema;
  }
}

/**
 * Implements hook_form_alter().
 */
function entity_distribution_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form_helper = \Drupal::service('entity_distribution.form_helper');
  if (!$form_helper->isSupportedFormId($form_id)) {
    return;
  }
  $form_helper->alterEntityTypeForm($form, $form_state, $form_id);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function entity_distribution_form_remote_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $request = \Drupal::request();
  $remote = $form_state->getFormObject()->getEntity();
  $form['url']['#default_value'] = $remote->get('url') ?: $request->get('url');
}

/**
 * Implements hook_jsonapi_resource_entity_links_alter().
 */
function entity_distribution_jsonapi_resource_entity_links_alter(LinkCollection &$links, $context) {
  $entity = $context['entity'] ?? NULL;

  // Add canonical into jsonapi links.
  if ($entity instanceof EntityInterface && $entity->hasLinkTemplate('canonical')) {
    $links = $links->withLink(
      'canonical',
      new Link(new CacheableMetadata(), $entity->toUrl(), 'canonical')
    );
  }
}
