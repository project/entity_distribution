<?php

namespace Drupal\entity_distribution\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * Class EntityDistribution.
 *
 * @package Drupal\entity_distribution\Plugin\DataType
 *
 * @DataType(
 *   id = "entity_distribution",
 *   label = @Translation("Entity Distribution")
 * )
 */
class EntityDistribution extends StringData implements EntityDistributionInterface {

}
