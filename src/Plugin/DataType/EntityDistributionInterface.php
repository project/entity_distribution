<?php

namespace Drupal\entity_distribution\Plugin\DataType;

use Drupal\Core\TypedData\PrimitiveInterface;

/**
 * Interface EntityDistributionInterface.
 *
 * @package Drupal\entity_distribution\Plugin\DataType
 */
interface EntityDistributionInterface extends PrimitiveInterface {

}
