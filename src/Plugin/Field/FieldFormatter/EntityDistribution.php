<?php

namespace Drupal\entity_distribution\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Class EntityDistribution.
 *
 * @package Drupal\entity_distribution\Plugin\Field\FieldFormatter
 *
 * @FieldFormatter(
 *   id = "entity_distribution_empty_formatter",
 *   module = "entity_distribution",
 *   label = @Translation("Empty formatter"),
 *   field_types = {
 *     "entity_distribution"
 *   }
 * )
 */
class EntityDistribution extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything.
    return [];
  }

}
