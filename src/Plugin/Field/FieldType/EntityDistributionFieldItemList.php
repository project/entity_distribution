<?php

namespace Drupal\entity_distribution\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Class EntityDistributionFieldItemList.
 *
 * @package Drupal\entity_distribution\Plugin\Field\FieldType
 */
class EntityDistributionFieldItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   */
  public function hasAffectingChanges(FieldItemListInterface $original_items, $langcode) {
    $normalized_items = clone $this;
    $normalized_original_items = clone $original_items;

    $normalized_items->preSave();
    $normalized_items->filterEmptyItems();
    $normalized_original_items->preSave();
    $normalized_original_items->filterEmptyItems();

    return !$normalized_items->equals($normalized_original_items);
  }

}
