<?php

namespace Drupal\entity_distribution\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Class EntityDistributionFieldItem.
 *
 * @package Drupal\entity_distribution\Plugin\Field\FieldType
 *
 * @FieldType(
 *   id = "entity_distribution",
 *   label = @Translation("Entity Distribution"),
 *   description = @Translation("This field stores code entity distribution."),
 *   list_class = "\Drupal\entity_distribution\Plugin\Field\FieldType\EntityDistributionFieldItemList",
 *   category = @Translation("Entity Distribution"),
 *   default_widget = "entity_distribution",
 *   default_formatter = "entity_distribution_empty_formatter",
 *   serialized_property_names = {
 *     "value"
 *   }
 * )
 */
class EntityDistributionFieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('entity_distribution')
      ->setLabel(t('Entity Distribution'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '' || $value === serialize([]);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $values = parent::getValue();

    if (isset($values['value']) && is_string($values['value'])) {
      $values = unserialize($values['value']);
    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    $keys = array_keys($this->definition->getPropertyDefinitions());
    $key = $keys[0];

    if (is_array($values) && !isset($values[$key])) {
      $values = serialize($values);
    }

    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();
    if (!is_string($this->value)) {
      $this->value = serialize($this->value);
    }
  }

}
