<?php

namespace Drupal\entity_distribution\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityDistribution.
 *
 * @package Drupal\entity_distribution\Plugin\Field\FieldWidget
 *
 * @FieldWidget(
 *   id = "entity_distribution",
 *   label = @Translation("Advanced entity distribution form"),
 *   field_types = {
 *     "entity_distribution"
 *   }
 * )
 */
class EntityDistribution extends WidgetBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $definition = new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings']);
    $definition->moduleHandler = $container->get('module_handler');
    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'sidebar' => TRUE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['sidebar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Place field in sidebar'),
      '#default_value' => $this->getSetting('sidebar'),
      '#description' => $this->t('If checked, the field will be placed in the sidebar on entity forms.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    if ($this->getSetting('sidebar')) {
      $summary['sidebar'] = $this->t('Use sidebar: Yes');
    }
    else {
      $summary['sidebar'] = $this->t('Use sidebar: No');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['#type'] = 'details';

    $item = $items[$delta];

    // Retrieve the values from the serialized array.
    $values = $item->getValue();

    // Invoke hook_entity_dist_entity_form_element_alter().
    $context = [
      'form' => $form,
      'form_state' => $form_state,
      'widget' => $this,
      'items' => $items,
      'default' => $this->isDefaultValueWidget($form_state),
    ];
    $this->moduleHandler->alter('entity_dist_entity_form_element', $element, $values, $context);

    // If the "sidebar" option was checked on the field widget, put the form
    // element into the form's "advanced" group. Otherwise, let it default to
    // the main field area.
    $sidebar = $this->getSetting('sidebar');
    if ($sidebar) {
      $element['#group'] = 'advanced';
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // $entity = $form_state->getFormObject()->getEntity();

    // if ($entity->isNew()) {
    //   $original_values = [];
    // }
    // else {
    //   $field_name = $this->fieldDefinition->getName();
    //   $original_values = $entity->get($field_name)->getValue();
    // }

    foreach ($values as $delta => &$value) {
      // $value += ($original_values[$delta] ?? []);
      $value = serialize($value);
    }

    return $values;
  }

}
