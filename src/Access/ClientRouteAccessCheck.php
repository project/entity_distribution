<?php

namespace Drupal\entity_distribution\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;

/**
 * Class ClientRouteAccessCheck.
 *
 * @package Drupal\entity_distribution\Access
 */
class ClientRouteAccessCheck implements AccessInterface {

  const CLIENT_ENTITIES = [
    'import_config',
    'entity_import_status',
  ];

  const CLIENT_ROUTE = [
    'entity_share_client.admin_content_page',
    'entity_share_client.admin_content_pull_form',
  ];

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ClientRouteAccessCheck constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account = NULL) {
    if (!$this->moduleHandler->moduleExists('entity_distribution_client')) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

}
