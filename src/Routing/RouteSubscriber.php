<?php

namespace Drupal\entity_distribution\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\entity_distribution\Access\ClientRouteAccessCheck;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\entity_distribution\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($collection->all() as $route_name => $route) {
      // Client entities.
      $preg_pattern = '/^entity\.(' . implode('|', ClientRouteAccessCheck::CLIENT_ENTITIES) . ')\./';
      if (preg_match($preg_pattern, $route_name)) {
        $route->setRequirement('_ed_client_route_access', 'TRUE');
      }
      // Client routing.
      if (in_array($route_name, ClientRouteAccessCheck::CLIENT_ROUTE)) {
        $route->setRequirement('_ed_client_route_access', 'TRUE');
      }
    }
  }

}
