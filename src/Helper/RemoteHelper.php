<?php

namespace Drupal\entity_distribution\Helper;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_distribution\Form\SettingForm;
use Drupal\entity_share_client\Entity\Remote;
use Drupal\entity_share_client\Entity\RemoteInterface;

/**
 * Class EntityShareHelper.
 *
 * @package Drupal\entity_distribution\Helper
 */
class RemoteHelper implements RemoteHelperInterface {

  /**
   * The remote websites.
   *
   * @var array
   */
  protected $remoteWebsites = [];

  /**
   * The site path.
   *
   * @var string
   */
  protected $sitePath;

  /**
   * The site name.
   *
   * @var string
   */
  protected $siteName;

  /**
   * The current website.
   *
   * @var \Drupal\entity_share_client\Entity\RemoteInterface
   */
  protected $currentWebsite;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * EntityShareHelper construction.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, $site_path, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->sitePath = $site_path;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getSiteName() {
    if (!$this->siteName) {
      $this->siteName = str_replace('sites/', '', $this->sitePath);
    }
    return $this->siteName;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentWebsite($throw = FALSE) {
    if (!$this->currentWebsite) {
      $remote_id = $this->configFactory->get(SettingForm::CONFIG_NAME)->get('current_website') ?: 0;
      $this->currentWebsite = Remote::load($remote_id);
    }

    if (!$this->currentWebsite && $throw) {
      throw new \Exception('Missing current website, please register.');
    }

    return $this->currentWebsite;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteWebsites($exclude_current = FALSE) {
    if (!$this->remoteWebsites) {
      $this->remoteWebsites = $this->entityTypeManager->getStorage('remote')->loadMultiple();
    }

    if (!$exclude_current) {
      return $this->remoteWebsites;
    }

    return array_filter($this->remoteWebsites, function (RemoteInterface $remote) {
      if ($remote->id() === $this->getCurrentWebsite()->id()) {
        return FALSE;
      }
      return TRUE;
    });
  }

}
