<?php

namespace Drupal\entity_distribution\Helper;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\jsonapi\ResourceType\ResourceTypeRepositoryInterface;
use Drupal\jsonapi_extras\Entity\JsonapiResourceConfig;
use Drupal\jsonapi_extras\ResourceType\ConfigurableResourceType;
use Drupal\jsonapi_extras\ResourceType\NullJsonapiResourceConfig;

/**
 * Class EntityHelper.
 *
 * @package Drupal\entity_distribution\Helper
 */
class EntityHelper implements EntityHelperInterface {

  /**
   * The resource type repository.
   *
   * @var \Drupal\jsonapi\ResourceType\ResourceTypeRepositoryInterface
   */
  protected $resourceTypeRepository;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  public function __construct(ResourceTypeRepositoryInterface $resource_type_repository, EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    $this->resourceTypeRepository = $resource_type_repository;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function createField($entity_type_id, $entity_bundle, $field_name) {
    // Create field storage.
    $field_storage = FieldStorageConfig::loadByName($entity_type_id, $field_name);
    if (!$field_storage) {
      $field_storage = FieldStorageConfig::create([
        'entity_type' => $entity_type_id,
        'field_name' => 'entity_distribution',
        'type' => 'entity_distribution',
        'locked' => TRUE,
        'cardinality' => 1,
      ]);
      $field_storage->save();
    }

    // Create field.
    $field = FieldConfig::loadByName($entity_type_id, $entity_bundle, $field_name);
    if (!$field) {
      $field = FieldConfig::create([
        'field_storage' => $field_storage,
        'bundle' => $entity_bundle,
        'label' => t('Entity Distribution'),
        'translatable' => TRUE,
      ]);
      $field->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createJsonapiResourceConfig($entity_type_id, $entity_bundle) {
    $resource_type = $this->resourceTypeRepository->get($entity_type_id, $entity_bundle);

    if (!$resource_type instanceof ConfigurableResourceType) {
      return;
    }

    $resource_config = $resource_type->getJsonapiResourceConfig();

    // Create jsonapi resource config.
    if ($resource_config instanceof NullJsonapiResourceConfig) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

      $resource_fields = [];
      foreach ($this->getAllFieldNames($entity_type, $entity_bundle) as $field_name) {
        $resource_fields[$field_name] = [
          'fieldName' => $field_name,
          'publicName' => $field_name,
          'enhancer' => ['id' => ''],
          'disabled' => FALSE,
        ];
      }

      $resource_config = JsonapiResourceConfig::create([
        'langcode' => $this->languageManager->getDefaultLanguage()->getId(),
        'id' => "{$entity_type_id}--{$entity_bundle}",
        'disabled' => FALSE,
        'path' => "{$entity_type_id}/{$entity_bundle}",
        'resourceType' => "{$entity_type_id}--{$entity_bundle}",
        'resourceFields' => $resource_fields,
      ]);
    }

    $resource_fields = $resource_config->get('resourceFields');

    // Set formatted text enhancer.
    if (isset($resource_fields['body']['enhancer']['id'])) {
      $resource_fields['body']['enhancer']['id'] = 'entity_share_embedded_entities';
    }

    // Set layout_builder enhancer.
    if (isset($resource_fields['layout_builder__layout']['enhancer']['id'])) {
      $resource_fields['layout_builder__layout']['enhancer']['id'] = 'entity_share_layout_builder';
    }

    // Set layout_builder__translation enhancer.
    // This field from layout_builder_st module.
    if (isset($resource_fields['layout_builder__translation']['enhancer']['id'])) {
      $resource_fields['layout_builder__translation']['enhancer']['id'] = 'entity_share_layout_builder';
    }

    $resource_config->set('resourceFields', $resource_fields);

    $resource_config->save();
  }

  /**
   * Gets all field names for a given entity type and bundle.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type for which to get all field names.
   * @param string $bundle
   *   The bundle for which to get all field names.
   *
   * @todo This is a copy of ResourceTypeRepository::getAllFieldNames. We can't
   * reuse that code because it's protected.
   *
   * @return string[]
   *   All field names.
   */
  protected function getAllFieldNames(EntityTypeInterface $entity_type, $bundle) {
    if (is_a($entity_type->getClass(), FieldableEntityInterface::class, TRUE)) {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions(
        $entity_type->id(),
        $bundle
      );
      return array_keys($field_definitions);
    }
    elseif (is_a($entity_type->getClass(), ConfigEntityInterface::class, TRUE)) {
      // @todo Uncomment the first line, remove everything else once https://www.drupal.org/project/drupal/issues/2483407 lands.
      // return array_keys($entity_type->getPropertiesToExport());
      $export_properties = $entity_type->getPropertiesToExport();
      if ($export_properties !== NULL) {
        return array_keys($export_properties);
      }
      else {
        return ['id', 'type', 'uuid', '_core'];
      }
    }

    return [];
  }

}
