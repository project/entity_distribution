<?php

namespace Drupal\entity_distribution\Helper;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Http\RequestStack;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Class FormHelper.
 *
 * @package Drupal\entity_distribution\Helper
 */
class FormHelper {

  use StringTranslationTrait;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The remote helper.
   *
   * @var \Drupal\entity_distribution\Helper\RemoteHelperInterface
   */
  protected $remoteHelper;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * FormHelper construction.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler, RemoteHelperInterface $remote_helper, RequestStack $request_stack) {
    $this->moduleHandler = $module_handler;
    $this->remoteHelper = $remote_helper;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * The supported form ids.
   *
   * @var array
   */
  protected $supportedFormIds = [];

  /**
   * Get the supported entity types.
   *
   * @TODO Add support for more types, now just for node type.
   *
   * @return string[]
   */
  public function getSupportedEntityTypes() {
    return ['node'];
  }

  /**
   * Get the supported form id.
   *
   * @return string[]
   */
  public function getSupportedFormIds() {
    if (!$this->supportedFormIds) {
      foreach ($this->getSupportedEntityTypes() as $entity_type) {
        $this->supportedFormIds[] = "{$entity_type}_type_edit_form";
      }
    }
    return $this->supportedFormIds;
  }

  /**
   * Check is supported form id.
   *
   * @param string $form_id
   *   The form id.
   *
   * @return bool
   */
  public function isSupportedFormId(string $form_id) {
    return in_array($form_id, $this->getSupportedFormIds());
  }

  /**
   * Alter entity type form.
   *
   * @param $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param $form_id
   *   The form id.
   */
  public function alterEntityTypeForm(&$form, FormStateInterface $form_state, $form_id) {
    $form_object = $form_state->getFormObject();

    if (!$form_object instanceof EntityFormInterface || !($type = $form_object->getEntity()) || !($type instanceof ConfigEntityInterface)) {
      return;
    }

    $current_website = $this->remoteHelper->getCurrentWebsite();

    $form['entity_distribution'] = [
      '#type' => 'details',
      '#title' => t('Entity Distribution'),
      '#group' => 'additional_settings',
      '#attributes' => ['class' => ['entity-distribution-fieldset']],
      '#attached' => [
        'library' => ['entity_distribution/fieldset_summaries']
      ],
    ];

    $messages = [];

    // Messages for remote websites.
    if (empty($current_website)) {
      $messages['error'][] = $this->t('Click <a href=":remote-add-form" target="_blank">here</a> to register current website.', [
        ':remote-add-form' => Url::fromRoute('entity.remote.add_form', [], [
          'query' => [
            'url' => Url::fromRoute('<front>')->setAbsolute()->toString()
          ]
        ])->toString(),
      ]);
    }

    $form['entity_distribution']['messages'] = [
      '#theme' => 'status_messages',
      '#message_list' => $messages,
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'warning' => t('Warning message'),
      ],
    ];

    $form['entity_distribution']['enable'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable entity distribution for this type'),
      '#parents' => ['entity_distribution', 'enable'],
      '#default_value' => $type->getThirdPartySetting('entity_distribution', 'enable', FALSE)
    ];

    $form['#entity_builders'][] = [static::class, 'entityTypeFormBuilder'];

    // Invoke hook_entity_dist_entity_type_form_element_alter().
    $context = [
      'form_state' => $form_state,
      'form_id' => $form_id,
    ];
    $this->moduleHandler->alter('entity_dist_entity_type_form_element', $form, $type, $context);
  }

  /**
   * The entity type form builder.
   *
   * @param $entity_type
   *   The entity type.
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $type
   *   The type.
   * @param $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function entityTypeFormBuilder($entity_type_id, ConfigEntityInterface $type, &$form, FormStateInterface $form_state) {
    // Save config into third party setting.
    $enable = $form_state->getValue(['entity_distribution', 'enable'], FALSE);
    $type->setThirdPartySetting('entity_distribution', 'enable', $enable);

    // Not do anything if entity distribution disabled.
    if (!$enable) {
      return;
    }

    $remote_helper = \Drupal::service('entity_distribution.remote_helper');
    $entity_type = $type->getEntityType();
    $entity_type_id = $entity_type->getBundleOf();
    $entity_bundle = $type->id();
    $current_website = $remote_helper->getCurrentWebsite();

    // Validate remote websites.
    if (empty($current_website)) {
      $form_state->setError(
        $form['entity_distribution']['enable'],
        t('Click <a href=":global-setting-form" target="_blank">here</a> to register current website for entity distribution.', [
          ':global-setting-form' => Url::fromRoute('entity_distribution.global_settings')->toString(),
        ])
      );
    }

    if ($entity_type_id && $entity_bundle) {
      $entity_helper = \Drupal::service('entity_distribution.entity_helper');
      // Create entity_distribution field.
      $entity_helper->createField($entity_type_id, $entity_bundle, 'entity_distribution');
      // Create jsonapi resource config.
      $entity_helper->createJsonapiResourceConfig($entity_type_id, $entity_bundle);
    }
  }

}
