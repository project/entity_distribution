<?php

namespace Drupal\entity_distribution\Helper;

/**
 * Interface RemoteHelperInterface.
 *
 * @package Drupal\entity_distribution\Helper
 */
interface RemoteHelperInterface {

  /**
   * The site name.
   *
   * @return string
   */
  public function getSiteName();

  /**
   * Get the current website.
   *
   * @param bool $throw
   *   Throw error or not.
   *
   * @return \Drupal\entity_share_client\Entity\RemoteInterface|NULL
   */
  public function getCurrentWebsite($throw = FALSE);

  /**
   * Get the remote websites.
   *
   * @param bool $exclude_current
   *   Exclude current site.
   *
   * @return \Drupal\entity_share_client\Entity\RemoteInterface[]
   */
  public function getRemoteWebsites($exclude_current = FALSE);

}
