<?php

namespace Drupal\entity_distribution\Helper;

/**
 * Interface EntityHelperInterface.
 *
 * @package Drupal\entity_distribution\Helper
 */
interface EntityHelperInterface {

  /**
   * Create entity field.
   *
   * @param $entity_type
   *   The entity type.
   * @param $entity_bundle
   *   The entity bundle.
   * @param $field_name
   *   The field name.
   */
  public function createField($entity_type, $entity_bundle, $field_name);

  /**
   * Create jsonapi resource config.
   *
   * @param $entity_type_id
   *   The entity type id.
   * @param $entity_bundle
   *   The entity bundle.
   */
  public function createJsonapiResourceConfig($entity_type_id, $entity_bundle);

}
