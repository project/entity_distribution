<?php

namespace Drupal\entity_distribution\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_share_client\Entity\RemoteInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingForm.
 *
 * @package Drupal\entity_distribution\Form
 */
class SettingForm extends ConfigFormBase {

  const CONFIG_NAME = 'entity_distribution.settings';

  /**
   * The remote helper.
   *
   * @var \Drupal\entity_distribution\Helper\RemoteHelperInterface
   */
  protected $remoteHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->remoteHelper = $container->get('entity_distribution.remote_helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_distribution_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $setting = $this->config(static::CONFIG_NAME);

    $remote_websites = $this->remoteHelper->getRemoteWebsites();

    $messages = [];

    if (empty($remote_websites)) {
      $messages['error'][] = $this->t('Click <a href=":remote-add-form" target="_blank">here</a> to register current website.', [
        ':remote-add-form' => Url::fromRoute('entity.remote.add_form', [], [
          'query' => [
            'url' => Url::fromRoute('<front>')->setAbsolute()->toString()
          ]
        ])->toString(),
      ]);
    }

    $form['messages'] = [
      '#theme' => 'status_messages',
      '#message_list' => $messages,
      '#status_headings' => [
        'status' => t('Status message'),
        'error' => t('Error message'),
        'warning' => t('Warning message'),
      ],
    ];

    $form['current_website'] = [
      '#type' => 'select',
      '#title' => $this->t('Current website'),
      '#options' => array_map(function (RemoteInterface $remote) {
        return $remote->label();
      }, $remote_websites),
      '#default_value' => $setting->get('current_website'),
      '#element_validate' => [[static::class, 'validateCurrentWebsite']],
      '#required' => TRUE,
      '#description' => $this->t('Click <a href=":remote-collection" target="_blank">here</a> to administer remote websites.', [
        ':remote-collection' => Url::fromRoute('entity.remote.collection')->toString(),
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(static::CONFIG_NAME)
      ->set('current_website', $form_state->getValue('current_website', NULL))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Validate the default channel.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateCurrentWebsite(array $element, FormStateInterface $form_state) {
    $remote_helper = \Drupal::service('entity_distribution.remote_helper');
    $remote_websites = $remote_helper->getRemoteWebsites();

    if (!in_array($element['#value'], array_keys($remote_websites))) {
      $form_state->setError($element, t('Invalid website.'));
    }
  }

}
